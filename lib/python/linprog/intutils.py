import re
int_rex = re.compile(r"^\s*-?\d+\s*$")

def integerish(val):
    copy=str(val)
    return re.match(int_rex, copy) != None

def select_bit(intval, bit):
    return (intval & 1<<bit) >> bit

def sanitize_index(v, out_format='gmpl'):
    if integerish(v) or  out_format == 'lp':
        return str(v)
    else:
        return "'{0:s}'".format(v)
