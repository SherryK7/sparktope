# as opposed to sparks variable
from linprog.intutils import sanitize_index

class LPVariable:
    def __init__(self, name, type='array', indices=[]):
        self.name=name
        self.type=type
        self.indices=indices

    def __trunc__(self):
        if self.type=='int':
            return int(self.name)
        else:
            raise TypeError("Cannot make int from {0:s}". \
                            format(self.name))

    def __str__(self):
        return self.stringify()

    def stringify(self,format='gmpl'):
        if self.type in ['scalar', 'int']:
            return str(self.name)

        if format=='gmpl':
            prefix="["
            sep=","
            suffix="]"
        else:
            prefix='#'
            sep='#'
            suffix=''

        return str(self.name)+prefix \
            +sep.join([sanitize_index(n,format) for
                       n in self.indices]) + suffix

    def parameterize(self,base,format='gmpl'):
        if self.type == 'int':
            index = str(self.name)
        else:
            index = self.stringify('gmpl')

        if format == 'gmpl':
            fmtstr= "{n:s}['{i:s}']"
        else:
            fmtstr= "${{{n:s}['{i:s}']}}"

        return fmtstr.format(n=base,i=index)

    @staticmethod
    def from_string(str):
        import re
        match=re.match(r"([^[]+)[[]([\d,]+)[]]",str)
        if match:
            indices=match.group(2).split(',')
            if len(indices)>1:
                type='matrix'
            else:
                type='array'
            return LPVariable(match.group(1),type,indices)
        else:
            return LPVariable(str,'scalar')            

        


    
