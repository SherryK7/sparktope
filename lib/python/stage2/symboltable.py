# simple symbol table, assigns keys successive integers
# counting from 1

from linprog.intutils import *
from linprog.LPVariable import LPVariable

class Symbol:
    def __init__(self,name,index,type,typeparams):
        self.name=name
        self.index=index
        self.type=type
        self._used={}
        self.typeparams=typeparams


    def mark_used(self, type, where):
        if not type in self._used:
            self._used[type] = {}

        self._used[type][where]=True

    def used(self, type):
        if type in self._used:
            return sorted(self._used[type].iterkeys())
        else:
            return []

    def __str__(self):
        return ("({name:s},{index:d},{type:s}," + \
                "{typeparams:s},{used:s})"). \
                    format(name=self.name, index=self.index,
                           type=self.type,
                           typeparams=self.typeparams,
                           used=self._used)

class SymbolTable:
    def __init__(self,word_size):
        self.count = 1
        self.word_size=word_size
        self.symbols = {}

    # support table[key] notation
    def __getitem__(self, key):
        return self.symbols[str(key)]

    def __contains__(self, key):
        return self.symbols.__contains__(str(key))

    def __len__(self):
        return self.symbols.__len__()
    def __iter__(self):
        return self.symbols.__iter__()

    def __iterkeys__(self):
        return self.symbols.__iterkeys__()

    def iteritems(self):
        return self.symbols.iteritems()

    def itervalues(self):
        return self.symbols.itervalues()

    # next index to be assigned
    def next(self):
        return self.count

    # skip this index
    def skip(self):
        self.count += 1

    # add a new symbol, if not already known
    def declare(self,rawkey,type,typeparams=[]):
        key=str(rawkey)
        if key not in self.symbols:
            self.symbols[key] = Symbol(key,self.count,type,typeparams)
            self.count += 1
        elif type in ['array', 'matrix']:
            self.symbols[key].typeparams = [ max(a,b) for a,b in zip(typeparams, self.symbols[key].typeparams) ]

        return self.symbols[key]

    def typeof(self,rawkey):
        key = str(rawkey)
        return self.symbols[key].type

    def sizeof(self,key):
        type = self.symbols[key].type
        if type == 'bool':
            return 1
        elif type == 'int':
            return self.word_size
        elif type == 'array':
            return self.symbols[key].typeparams[0]
        else:
            # matrix
            return self.symbols[key].typeparams[:2]

    def isTriangular(self,key):
        return self.symbols[key].type == 'matrix' and \
            'triangular' in self.symbols[key].typeparams

class SymbolTableTable:

    def __init__(self, word_size):
        self.tables = {}
        self._prefix = {}
        self.word_size = word_size

    def __getitem__(self,prefix):
        return self.tables[prefix]

    def add(self, prefix):
        self.tables[prefix] = SymbolTable(self.word_size)

    def declare(self, prefix, symname, type, typeargs=[]):
        self._prefix[symname] = prefix
        self.tables[prefix].declare(symname, type, typeargs)

    def prefix(self, varname):
        if varname in self._prefix:
            return self._prefix[varname]
        else:
            return None

    def typeof(self, var):
        if integerish(var):
            return 'int'
        else:
            prefix = self.prefix(var)
            return self.tables[prefix].typeof(var)

    def sizeof(self, var):
        if integerish(var):
            return self.word_size
        else:
            prefix = self.prefix(var)
            return self.tables[prefix].sizeof(var)

    def isTriangular(self, var):
        if integerish(var):
            return False
        else:
            prefix = self.prefix(var)
            return self.tables[prefix].isTriangular(var)

    def array_at(self, var, index, time):
        if self.prefix(var) in ['input','output']:
            return LPVariable(var,'array',[index]);
        else:
            return LPVariable(var,'array',[index,time])


    def matrix_at(self, var, row, col, time):
        if self.prefix(var) in ['input','output']:
            return LPVariable(var,'array',[row,col])
        else:
            return LPVariable(var,'array',[row,col,time])

    def bool_at(self, val, time):
        if integerish(val):
            return val

        parts = val.split(':')
        var = parts[0]
        if len(parts)>1:
            idx = int(parts[1])
            if self.prefix(var) in ['input','output']:
                return LPVariable(var,'array',[idx])
            elif integerish(var):
                return select_bit(int(var),idx)
            else:
                return LPVariable(var,'array',[idx,time])
        else:
            if self.prefix(var) in ['input','output']:
                return LPVariable(var,'scalar')
            elif integerish(var):
                return var
            else:
                return LPVariable(var,'array',[time])


    def declare_array_temp(self, letter, count, arraymat, isCol=None):

        if isCol:
            index = 1
        else:
            index = 0

        type = self.typeof(arraymat)
        if type == 'matrix':
            size = self.sizeof(arraymat)[index]
        else:
            size = self.sizeof(arraymat)

        self.declare('internal',
                     '_{letter:s}{count:d}'.format(
                         letter=letter, count=count),
                     'array', [size])

    def split_at(self, var, time, type):

        if type=='bool':
            return [self.bool_at(var,time)]
        else:
            triangular = False

            if type=='int':
                rows = 1
                bit_count=self.word_size
            elif type == 'array':
                rows = 1
                bit_count=self.sizeof(var)
            else:
                # matrix
                triangular = self.isTriangular(var)
                pair = self.sizeof(var)
                bit_count = pair[1]
                rows = pair[0]

            out = []

            for row in range(0,rows):
                for bit in reversed(range(row+1 if triangular else 0,bit_count)):
                    if integerish(var):
                        out += [ select_bit(int(var), bit) ]
                    else:
                        if type=='matrix':
                            indices = [row]
                        else:
                            indices = []

                        indices += [bit]

                        if not self.prefix(var) in ['input','output']:
                            indices += [time]

                        term = LPVariable(var,'array',indices)

                        out += [ term ]

            return out
