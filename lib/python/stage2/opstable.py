import os
import json
from stage2.wordops import WordOps
from stage2.op import Op

class OpsTable:
    def __init__(self, bool_ops=None, word_ops=None, word_size=None,int_ops=None):

        self.ops = {}

        base_dir = os.path.dirname(__file__)

        files = [ bool_ops or \
                  os.path.join(base_dir,'..','..','ops','boolean.json'),
                  int_ops or \
                  os.path.join(base_dir,'..','..','ops','int.json')]

        if word_ops == None:
            wo = WordOps(word_size)

            self.ops['set_addw'] = wo.addw()
            self.ops['set_eqw'] = wo.eqw()
            self.ops['set_copyw'] = wo.copyw()
            self.ops['set_incw'] = wo.incw()
            self.ops['set_ltw'] = wo.ltw()
        else:
            files.append(word_ops)

        for file_name in files:
            with open(file_name) as file:
                ops = json.load(file)

            for key in ops:
                self.ops[key] = Op(name=key,**ops[key])

    def __getitem__(self, key):
            return self.ops[key]
