#
# for generating integer gates, enter w the number of bits
#

import sys
from stage2.op import Op

def write(*args):
    sys.stdout.write(*args)

def notwor(w, I, dest, n):
    '''
    dest=!(I[1] or ... or I[w])
    '''

    ineq = [];

    for k in range(1, w+1):
        row = [2, -1];
        for i in range(3, n+1):
            if (i == I[k]) or (i == dest):
                row.append(-1)
            else:
                row.append(0)
        ineq.append(row);

    row = [0, -1]
    for i in range(3, n+1):
        c = 0
        if i == dest:
            c = 1
        for k in range(1, w+1):
            if i == I[k]:
                c = 1
        row.append(c)
    ineq.append(row)
    return ineq

def not2or(a, b, dest, n):
                                # dest=a xor b
    write("\n2 -1")
    for i in range(3, n+1):
        if (i == a) or (i == dest):
            write(" -1")
        else:
            write("  0")
    write("\n2 -1")
    for i in range(3, n+1):
        if (i == b) or (i == dest):
            write(" -1")
        else:
            write("  0")
    write("\n0 -1")
    for i in range(3, n+1):
        if (i == b) or (i == a):
            write("  1")
        elif i == dest:
            write("  1")
        else:
            write("  0")

def nand(a, b, dest, n):
    '''
    dest=!a and b
    '''
    ineq = []
    row =[1, -1]
    for i in range(3, n+1):
        if (i == a) or (i == dest):
            row.append(1)
        elif i == b:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)

    row=[1, -1]

    for i in range(3, n+1):
        if i == b:
            row.append(1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)

    row=[2, -1]
    for i in range(3, n+1):
        if i == a:
            row.append(-1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)

    return ineq

def Or(a, b, dest, n):
    '''
    dest= a or b
    '''

    ineq = []

    row =[1, -1]
    for i in range(3, n+1):
        if (i == a) or (i == b):
            row.append(1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)

    row=[1,-1]
    for i in range(3, n+1):
        if i == b:
            row.append(-1)
        elif i == dest:
            row.append(1)
        else:
            row.append(0)
    ineq.append(row)

    row=[1, -1]
    for i in range(3, n+1):
        if i == a:
            row.append(-1)
        elif i == dest:
            row.append(1)
        else:
            row.append(0)
    ineq.append(row)

    return ineq

def And(a, b, dest, n):
    '''
    dest=a and b
    '''
    ineq = []
    row = [2,-1]
    for i in range(3, n+1):
        if (i == a) or (i == b):
            row.append(-1)
        elif i == dest:
            row.append(1)
        else:
            row.append(0)
    ineq.append(row)

    row=[1, -1]
    for i in range(3, n+1):
        if i == b:
            row.append(1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)

    row=[1, -1]
    for i in range(3, n+1):
        if i == a:
            row.append(1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)
    ineq.append(row)
    return ineq

def xor(a, b, dest, n):
                                # dest=a xor b
    ineq = []
    row = [1, -1]

    for i in range(3, n+1):
        if (i == b) or (i == dest):
            row.append(1)
        elif i == a:
            row.append(-1)
        else:
            row.append(0)

    ineq.append(row);
    row=[1,-1];

    for i in range(3, n+1):
        if (i == b) or (i == a):
            row.append(1)
        elif i == dest:
            row.append(-1)
        else:
            row.append(0)

    ineq.append(row)
    row=[1, -1]
    for i in range(3, n+1):
        if (i == a) or (i == dest):
            row.append(1)
        elif i == b:
            row.append(-1)
        else:
            row.append(0)

    ineq.append(row)
    row=[3, -1]
    for i in range(3, n+1):
        if (i == a) or (i == b) or (i == dest):
            row.append(-1)
        else:
            row.append(0)

    ineq.append(row)
    return ineq

def writebounds(n):
    write("\n")
    for i in range(2, n+1):
        for j in range(1, n+1):
            if j == 1:
                write("\n1")
            elif i == j:
                write(" -1")
            else:
                write("  0")
    write("\nend\n")

class WordOps:
    def __init__(self,w):
        self.bits=w

    def copyw(self):
        '''
        copy w-bit integer from x to y or vice versa
        '''
        w = self.bits
        n = 2 * w + 2

        O = Op(name='set_copyw',
               tag='WC',
               comment= "if S[i,t] then y = x",
               types= ["bool", "int", "int" ],
               columns=["S[i,t]",
                        "x{0:d} ... x1".format(w),
                        "y{0:d} ... x1".format(w)],
               inequalities=[]
        )

        for i in range(1, w+1):
            row = [1,-1]
            for j in range(1, 2 * w+1):
                if j == i:
                    row.append(1)
                elif j == (w + i):
                    row.append(-1)
                else:
                    row.append(0)
            O.inequalities.append(row)
            row=[1, -1]
            for j in range(1, 2 * w+1):
                if j == i:
                    row.append(-1)
                elif j == (w + i):
                    row.append(1)
                else:
                    row.append(0)
            O.inequalities.append(row)

        return O

    def addw(self):
        '''
        z=x+y with carry c
        '''

        w = self.bits

        x=[0]; y=[0]; z=[0]; c=[0];     # locations of x,y,z,c
        R=[0]; S=[0]; T=[0];            # locations of temps

        n = 7 * w - 1 ;

        # we compute the location of high order "w" bit of each integer
        # note integers are stored  x_w ... x_1
        for i in range(1,w+1):
            x.append(i + 2)
            y.append(i + w + 2)
            z.append(i + 2*w + 2)
            c.append(i + 3*w + 2)
            R.append(i + 4*w + 2)
            S.append(i + 5*w + 1)
            T.append(i + 6*w)

        ineq = []

        ineq+= xor(x[w],y[w],z[w],n)
        ineq+= And(x[w],y[w],c[w],n)

        for i in range(w-1,0,-1):
            ineq += And(x[i],y[i],R[i],n)
            ineq += xor(x[i],y[i],S[i],n);
            ineq += And(c[i+1],S[i],T[i],n);
            ineq += xor(S[i],c[i+1],z[i],n);
            ineq += Or(R[i],T[i],c[i],n);

        return Op(name='set_addw',
                  tag='WA',
                  comment = "if S[i,t] then z = x + y",
                  types = ["bool", "int", "int", "temps", "int" ],
                  temp_bits = 4*w - 3,
                  columns=["S[i,t]",
                           "x{0:d} ... x1 y{0:d} ... y1 z{0:d} ... z1".format(w),
                           "c{0:d} ... c1 r{0:d} ... r2 s{0:d} ... s2 t{0:d} ... s2".format(w)],
                  inequalities=ineq)

    def incw(self):
        #
    # increment I to J with J=0 on overflow
    #
        w = self.bits
        n = 3 * w
        R= range(0, w+1)
        I = [ w - i + 3 for i in R ]
        J = [ 2 * w - i + 3 for i in R ]
        tmp = [ 2 * w + 3 for i in R ]

        O = Op(name='set_incw',
               tag='WI',
               comment = "if S[i,t] then y = x + 1",
               types= ["bool", "int", "int" ],
               temp_bits = w - 2,
               columns=["S[i,t]",
                        "x{0:d} ... x1".format(w),
                        "y{0:d} ... x1".format(w),
                        "temp1 ... temp{0:d}".format(w-2)],
               inequalities=[]
        )

        row=[0,-1]
        for i in range(3, n+1):
            if i == (w + 2):
                row.append(1)
            elif i == (2 * w + 2):
                row.append(1)
            else:
                row.append(0)

        O.inequalities.append(row)

        row = [2, -1]
        for i in range(3, n+1):
            if i == (w + 2):
                row.append(-1)
            elif i == (2 * w + 2):
                row.append(-1)
            else:
                row.append(0)
        O.inequalities.append(row)

        O.inequalities += xor(w + 1, w + 2, 2 * w + 1, n)
        if w>2:
            O.inequalities += And(w + 1, w + 2, 2 * w + 3, n)

        for i in range(w - 2):
            O.inequalities += xor(w - i, 2 * w + 3 + i, 2 * w - i, n)
            if i < w - 3:
                O.inequalities += And(w - i, 2 * w + 3 + i, 2 * w + 4 + i, n)

        return O

    def ltw(self):
        #
    # dest = (I<J) ie. 1 if less than and zero otherwise
    #
        w = self.bits
        n = 6 * w - 1


        R = range(0, w+2)
        I  = [ w - i + 3 for i in R ]
        J  = [ 2 * w - i + 3 for i in R ]
        B  = [ i + 2 * w + 2 for i in R ]
        B1 = [ i + 3 * w + 1 for i in R ]
        B2 = [ i + 4 * w for i in R ]
        B3 = [ i + 5 * w - 1 for i in R ]

        I[1] = w + 2
        J[1] = 2 * w + 2
        B[w + 1] = 2 * w + 3	# output bit

        ineq = []
        ineq += nand(I[1], J[1], B[2], n)

        for i in range(2, w+1):
            ineq += nand(I[i], J[i], B1[i], n)
            ineq += xor(I[i], J[i], B2[i], n)
            ineq += nand(B2[i], B[i], B3[i], n)
            ineq += Or(B1[i], B3[i], B[i + 1], n)

        return Op(name='set_ltw',
               tag='WL',
               comment = "if S[i,t] then z = (x < y)",
               types = ["bool", "int", "int", "temps", "bool" ],
               temp_bits = 4*w - 4,
               columns=["S[i,t]",
                        "x{0:d} ... x1".format(w),
                        "y{0:d} ... x1".format(w),
                        "B2..B{0:d}"
                        "B2..B%d B12..B1%d B22..B2%d B32..B3%d" % (w,w,w,w),
                        "temp1 ... temp{0:d}".format(4*w-4)],
               inequalities=ineq
        )



        return O

    def eqw(self):
        '''
        dest = (I==J) ie. 1 if equal and zero otherwise
        '''
        w = self.bits
        n = 3 * w + 3

        O = Op(name='set_eqw',
               tag='WE',
               comment="if S[i,t] then z = (x == y)",
               types=["bool", "int", "int", "temps", "bool"],
               temp_bits = w,
               columns=["S[i,t]",
                        "x{0:d} ... x1".format(w),
                        "y{0:d} ... x1".format(w),
                        "z",
                        "temp1 ... temp{0:d}".format(w)],
               inequalities=[]
        )

        R = range(0, w+1)
        I  = [  i + 2 for i in R ]
        J =  [  i + w + 2 for i in R ]
        tmp = [ i + 2 * w + 3 for i in R ]

        for i in range(1, w+1):
            O.inequalities += xor(I[i], J[i], tmp[i], n)

        O.inequalities += notwor(w, tmp, 2 * w + 3, n)

        return O
