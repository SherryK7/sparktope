This is the package "sparktope", a compiler to generate linear
programs from algorithms.

Installation instructions are in doc/INSTALL

User documentation is in doc/manual.tex or precompiled in the
[wiki](https://gitlab.com/sparktope/sparktope/wikis/User-manual).

