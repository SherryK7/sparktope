#include <stdio.h>
#include <stdlib.h>

/*n machine scheduling with m jobs, time of job i = p[i]+1 */
/* T is the makespan                                       */

int main (int ac, char **av)
{
    int i, j, T, last, proc;
    int n,m;
    int single;
    int p[100];
    int x[100][100];   /*x[i][j]=1 if job i assigned to machine j*/

    T=0;
    proc=n-1;
    last=1;
    single=0;   /* =1 schedule 1 job per machine */

    scanf(" %d %d",&n,&m);
    printf("\n %d %d\n",n,m);

    for (i=0;i<m;i++)
      {
       scanf(" %d ",&p[i]);
       printf(" %d",p[i]+1);
       for (j=0;j<n;j++)
          x[i][j]=0;
      }


/* sched 2's */
    for (i=0;i<m;i++)
     if(p[i]==1) /* job time of 2 */
     {
       proc++;
       if (proc == n)
          proc=0;
       x[i][proc]=1;
       if(proc==0)
          T=T+2;
     }
/* sched 1's */
    last=proc+1;     /*last processor withlength 2 job or zero*/
    if(last ==n )
       last=0;       /*schedule 1 job per machine */

    for (i=0;i<m;i++)
     if(p[i]==0) /* job time of 1 */
     {
       proc++;
       if (n == proc)
         {
          if(single)
            last=0;      /* finished double scheduling ones */
          proc=last;
          single=1;
          if(last==0)
             T++;
         }
       x[i][proc]=1;
     }

   for (i=0;i<m;i++)
       for (j=0;j<n;j++)
          if(x[i][j]==1) 
             printf("\njob %d on machine %d t=%d",i,j,p[i]+1);
   printf("\n T=%ld \n",T);

}

