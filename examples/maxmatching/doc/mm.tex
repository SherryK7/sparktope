\documentclass{article}
\usepackage[a4paper,scale=0.8]{geometry}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
%\usepackage{datetime}
%\usepackage{txfonts}
\usepackage{bm}  % Must be after txfonts
\usepackage{url}
\usepackage[numbers]{natbib}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{blkarray}
\usepackage{color}
\usepackage{bbm}
\usepackage{dsfont}
%\usepackage{caption}
\usepackage{subcaption}
\usepackage[toc,page]{appendix}
\usepackage{blkarray}

\newcommand{\glpsol}{\textsc{glpsol}}
\newcommand{\cplex}{\textsc{cplex}}
\newcommand{\gurobi}{\textsc{gurobi}}
\newcommand{\conv}{\mathop{\mathrm{conv}}}
\newcommand{\fs}{\mathop{\mathrm{fs}}} % fooling set
\newcommand{\rk}{\mathop{\mathrm{rank}}} % non-negative rank
\newcommand{\nnegrk}{\mathop{\mathrm{rank}_+}} % non-negative rank
\newcommand{\snnegrk}{\mathop{\mathrm{rank}_\oplus}} % non-negative rank
\newcommand{\sucnnegrk}{\mathop{\mathrm{rsucc}_+}} % non-negative rank
\newcommand{\psdrk}{\mathop{\mathrm{rank}}\nolimits_{PSD}} % non-negative rank
%\newcommand{\rc}{\mathop{\mathrm{rc}}} % rectangle covering bound
\newcommand{\supp}{\mathop{\mathrm{supp}}} % support
\newcommand{\suppmat}{\mathop{\mathrm{suppmat}}} % support
\newcommand{\vertexset}{\mathop{\mathrm{vert}}}
\newcommand{\xc}{\mathop{\mathrm{xc}}} % extension complexity
\newcommand{\proj}{\mathrm{proj}}

\newcommand{\Pp}{{\textsc{P}}}
\newcommand{\NP}{{\textsc{NP}}}
\newcommand{\PP}{{\textsc{P}}}
\newcommand{\coNP}{{\textsc{coNP}}}
\newcommand{\Ppoly}{{\textsc{P/poly}}}

\newcommand{\vct}{\bm}
\newcommand{\A}{{\mathtt{A}}}
\newcommand{\B}{{\mathrm{B}}}
\newcommand{\I}{{\mathrm{I}}}
\newcommand{\K}{{\mathrm{K}}}
\newcommand{\N}{{\mathrm{N}}}
\newcommand{\Tri}{\Delta}
\newcommand{\RR}{{\mathbb{R}}}
\newcommand{\trans}{{\mathrm{T}}}
\newcommand{\CUT}{{\mathrm{CUT}}}
\newcommand{\CutP}{\mathrm{CUT}^\square}
\newcommand{\Cor}{{\mathrm{COR}}}
\newcommand{\CorP}{\mathrm{COR}^\square}
\newcommand{\TSP}{{\mathrm{TSP}}}
\newcommand{\SAT}{{\mathrm{SAT}}}
\newcommand{\STAB}{{\mathrm{STAB}}}
\newcommand{\threeDM}{{\mathrm{3DM}}}
\newcommand{\SUBSETSUM}{{\mathrm{SUBSETSUM}}}
\newcommand{\Knapsack}{{\mathrm{KNAPSACK}}}
\newcommand{\COR}{{\mathrm{COR}}}
\newcommand{\EP}{\mathrm{EP}}
\newcommand{\PM}{\mathrm{PM}}
\DeclareMathOperator{\CH}{CH}
\newcommand{\symdiff}{\mathbin{\triangle}}
\newcommand*{\abs}[1]{\lvert#1\rvert}

%DB
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{sparklistings}
\usepackage{tcolorbox}
\DeclareMathOperator{\steps}{steps}
\newcommand{\Lower}{\textit{lower}}
\newcommand{\Upper}{\textit{upper}}
\newcommand{\Body}{\textit{body}}
\newcommand{\BoolExpr}{\textit{bool\_expr}}
\newtcolorbox{spasm}{colframe=red!75!black,sidebyside,lefthand width=1.6in}
\newcommand{\Sparks}{\textsc{sparks}}
\newcommand{\Sparktope}{\textsc{sparktope}}
\newcommand{\Asm}{\textsc{asm}}
\newcommand{\Sdot}[2]{\filldraw(#1,#2) node[rectangle,inner sep=2.5pt,stroke=black,fill=black!75]{};}

\usepackage{etoolbox}

\newlength{\profcol}
\setlength{\profcol}{1.3cm}
\newcommand{\rc}[4]{\hfill\makebox[4\profcol][l]{
    \small
    \makebox[1.0cm]{#1}\makebox[\profcol]{#2}\makebox[\profcol]{#3}\makebox[\profcol]{#4}
  }}


\usepackage[bordercolor=white]{todonotes}
\newcommand{\fixme}[1]{%
    \todo[noline,noinline,caption={#1}]{\textsc{fixme}}%
}

\usepackage{tcolorbox}
\tcbuselibrary{listings}
\newtcblisting{sparksbox}[1][]{listing only,listing remove caption=false,colback=black!3,listing options={language=sparks,frame=none,#1}}

\title{Analysis of the maximum matching code {\em mm.spk}}
\date{2020-05-07}

%\biboptions{longnamesfirst,angle,semicolon}
\bibliographystyle{abbrvnat}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\theoremstyle{remark}
\newtheorem{remark}{Remark}

%\let\cite\citep

\BAnewcolumntype{s}{>{$\small$}c}

\newlength{\normalparindent}
\setlength{\normalparindent}{\parindent}
\newlength{\normalparskip}
\setlength{\normalparskip}{\parskip}

\begin{document}

\maketitle
\footnotetext[1]{School of Computer Science, McGill University}

\footnotetext[2]{Graduate School of Informatics,
   Kyoto University}

\footnotetext[3]{Faculty of Computer Science, University of New Brunswick}


\lstset{language=sparks,frame=single}

\section{Maximum matching}
\label{matching}
We consider the following matching problem:

\begin{verbatim}
Instance: integer n, graph G and matching M in G.

Problem: Decide whether M is a maximum matching, and if it is not, 
         to find an augmenting path.

Output:  w=0 if M is a maximum matching
         w=1 if there is an augmenting path
\end{verbatim}

This standard formulation of this problem has exponential extension complexity, as follows from the
result of Rothvoss \cite{Rothvoss17}.
However, one iteration of Edmonds' blossom algorithm \cite{Edmonds1965a} can answer the above  problem
and this is achieved by the code \texttt{mm.spk} given in Appendix \ref{spkcode}.
It is based on the pseudocode in \cite{wikiBlossom} and a detailed explanation
and proof of correctness is given in Section 16.5 of Bondy and Murty \cite{BM2008}.
To produce linear programs for this problem we need to bound,
for each $n$, 
the maximum number of assembler steps taken for any instance size $n$.
We do this in the next section getting an upper bound of less than $21n^3$ steps.
Since the space required is $O(n^2 \log n)$
it follows from results in \cite{ABTW19} that the sparktope produced has $O(n^5 \log n)$
constraints. 


\subsection{Step count analysis}

For a given integer $n$ we will give an upper bound on the number of steps taken
by {\em mm.asm} in order to reach a return statement. Referring to the source code
 \texttt{mm.spk} we see that the program is divided into two phases to reduce the number
of constraints produced. Phase {\em init} handles initialization and is executed once
whereas phase {\em main} performs the blossom algorithm.

The loop structure of \texttt{phase} {\em init} is shown
in Figure \ref{init}. The line numbers correspond to the assembly code 
\texttt{mm.asm} and as the first 28 lines are declarations they are omitted. 
It is necessary to get both an upper and lower bound on the number of steps 
executed in \texttt{phase} {\em init}.
A total of 7 lines are executed once: 29-32, 38-39, and 59.
The first \texttt{for} loop in lines 33-38 has 6 steps but the \texttt{done} statement is only executed once
and steps 36 and 37 are executed $n-1$ times.
So this loop requires exactly $5n-1$ steps.
For simplicity in what follows a \texttt{for} loop with $k+1$ steps, including the \texttt{done}
statement,
is assigned an upper bound of $kn$ steps and a lower bound of
$(k-1)n$ steps. 

There remain two nested \texttt{for} loops in lines 20-59. The inner \texttt{for} loop in lines
42-54 is executed $n-1$ times with the corresponding number of iterations
$=n-1,n-2,...,1$.  Since the loop has 13 lines an upper bound on the
number of steps executed is therefore
$12(n-1+n-2+...+1)=6n(n-1)$.
The remaining 7 lines in the outer \texttt{for} loop
40-58 contribute at most $7n$ steps. In total we have an upper
bound of $7+6n^2-6n+7n=6n^2+n+1$ for \texttt{phase} {\em init}. 

For the lower bound we note that 
lines 47,48 are executed only for edges
in an input matching and there may be zero of those. So we reduce the size of
the inner loop by 3 getting a lower bound of $9n(n-1)/2$ steps.
For the outer loop we reduce its size to 6 so in total the lower bound is
$7+9n(n-1)/2+6n=(9n^2+3n+14)/2$ steps.

For $n=8$ we have an upper bound of 393 and a lower bound of 307 steps. Referring to the
output snapshot in Figure \ref{wt8} we see that 
S[59,364]=1 and
S[62,365]=1
so that for this input 364 steps were executed in \texttt{phase} {\em init}.

\begin{figure}

\begin{sparksbox}[basicstyle=\scriptsize]
     phase init do
29     
     ...one time assignments...
32   
33   for i<- 0,$n-1$ do
          .........
38   done
39   ...one time assignment...
40   for i<- 0,$n-2$ do
        .........
42	for j<-inc(i),$n-1$
          .........
          if a[j,i] then   # matching edge: j  i
47            match[[j]] <- i
48            match[[i]] <- j
          endif

54      done
        .........
59   done
     done                  # phase init
\end{sparksbox}

\caption{Control structure of phase init of \texttt{mm.spk} }
\label{init}
\end{figure}

\begin{figure}
\begin{sparksbox}[basicstyle=\scriptsize]
     phase main do
62   while progress do      #Loop A: find aug path and exit or find blossom and shrink
         ...
68       for i<-0,$n-1$ do
            ...
82       done
         ...
85       while !progress and !doneV do         #Loop B: process unexplored vertex V
         .      ...
100      .      while !progress and !doneW do  #Loop C: process unmarked edge VW
         .      .     ...
124      .      .   if  !F[W] then   # add edge to F
         .      .       ...
133      .      .   else             
                     .
                     .               # see Figure below
                     .
239      .      .   endif            
         .      .       ...
247      .      done                 # end of Loop C
         .      ...
255      done                        # end of Loop B     
257  done                            # end of Loop A  
258  return w @ 0                    # no augmenting matching
260  done                            # phase main
\end{sparksbox}
\caption{Control structure of phase main }
\label{main}
\end{figure}

\begin{figure}
\begin{sparksbox}[basicstyle=\scriptsize]
133  else
134        while i != parent[[i]] do    # Loop D
             ...
140        done
           ...
142        while k != parent[[k]] do    # Loop E
              ...
148        done
              ...
151        if  i != k then       # augmenting path
               return w @ 1
154        else                  # shrink blossom
               ...
157            while V != X  do         # Loop F
                   ...
165            done
                   ...
169            while V != W do   #Loop G: traverse and shrink cycle
               .       ...
196            .       while j<V do         # Loop H
               .         ...
213            .       done
214            .       while j != $n-1$ do  # Loop I
               .         ...
233            .       done
               .       ...

236            done          # traverse and shrink cycle
238        endif             # if i != k
                ...
\end{sparksbox}
\caption{Control structure of path and blossom processing }
\label{shrink}
\end{figure}

We now turn to the main part of the blossom algorithm and give the control structure in
Figure \ref{main}. There are three nested \texttt{while} loops labelled A,B,C respectively. 
The outer Loop A, lines
62-257, finds either an augmenting path or a blossom. 
A blossom is an odd cycle of
length at least three which is shrunk to a single vertex, removing at least 2 nodes
of the graph. Graphs cannot be shrunk to less than 3 vertices,
so shrinking can happen at most $(n-3)/2$ times and this is a bound on the
number of times Loop A can be executed. 
Loop B, lines 85-255 is executed
for each vertex in the (possibly shrunk) graph, so $n$ times at most
for each iteration of Loop A. 
Loop C, lines 100-247, is the third nested
loop, and is executed at most once for each vertex adjacent to the
vertex chosen in Loop B. 
So at most $n$ iterations are required for each iteration of Loop B.

We begin by analyzing the inner Loop C.
The block of code (line numbers 133-238) shown in Figure \ref{shrink}
either finds an augmenting path or handles blossom shrinking.
It can be executed at most once for each iteration of Loop A,
so $(n-3)/2$ times in total.
It is analyzed separately below. 
Removing these 106 lines from the 148 lines in Loop C
leaves 42 lines that require at most $42n$ time steps for each iteration of Loop B.
Moving to Loop B and we find 23 lines
(85-99, 248-255) which are not in Loop C.
Loop B executes $n$ times for each iteration of Loop A and so requires
at most $42n^2+23n$ time steps for such an iteration.

Finally in Loop A there is a \texttt{for} statement, lines 68-82, executed $n$ times for a total of
at most $15n$ steps per iteration.
There remain 10 lines (62-67, 83-84, 256-57)
%\todo{I guess the last range should be 256-257: yes} 
executed once for each iteration.
So adding in the steps for the inner loops (except shrinking)
an iteration of Loop A requires at most $42n^2+38n+10$ steps. 

We now turn to the code in Figure \ref{shrink} which is executed at most $(n-3)/2$ times.
Consider a single iteration of these 106 lines.
Loops D and E of 7 lines each can be executed at most $n$ times each for a total of $14n$ steps.
Inside the \texttt{else} clause beginning on line 155
are several more loops. Loop F, lines 157-165, is executed at most $n$ times for
a total of $9n$ steps. 
Loop G, lines 169-236, is more complex. Consider a single iteration.
There are two further
\texttt{while} loops, Loop H on lines 196-213 and Loop I on lines 214-233. 
Since $j$ increments every time H of I runs, together these are executed at
most $n$ times in an iteration of Loop G.
% \todo{Maybe a hint j increments every time H or I runs#done}. 
The second of these is longer and has 20 lines.
So both loops together take at most $20n$ steps for each of at most $n$ iterations,
or $20n^2$ steps in total.
The remaining 30 lines (169-195, 234-236) of Loop G are executed once per iteration.
The total number of steps taken in Loop G for a single blossom shrinking is 
therefore at most $20n^2+30n$.
%\todo{Did I miss something, or should this be $20n^2+30n$#correct!}
In this code block there remain lines 141, 149-156, 166-168, 238, or a total of 12 lines,
each of these is executed once per iteration of the code block.
It follows that the total number of steps taken in blossom shrinking is at most
$(14n+9n+20n^2+30n+12)=20n^2+53n+12$.

Putting everything together the total number of steps for an iteration of Loop A
is at most $62n^2+91n+22$. Since this loop executes at most $(n-3)/2$
times this gives an upper bound of $(62n^3-75n^2-251n-66)/2$ steps.
To this we add the upper bound on the steps taken in \texttt{phase} {\em init}, derived above,
of $6n^2+n+1$ obtaining a total of $(62n^3-63n^2-249n-64)/2$ steps.

\subsection{Examples}
\label{examples}

Two example inputs are provided in Appendix \ref{inputs} for the case $n=8$, \texttt{wt8} and \texttt{wt8a}.
The graphs have vertices labelled 0,1,...,7 and the input matching has 3 edges 01,23,45,
as shown for $wt8$ in Figure \ref{wt8_graphs} (a).

\begin{figure}[!ht]
\begin{subfigure}{.25\textwidth}
  \centering
  \includegraphics[width=.7\linewidth]{figs/graph-a.pdf}
  \caption{Graph and matching}
  \label{fig:sfig1}
\end{subfigure}%
\begin{subfigure}{.25\textwidth}
  \centering
  \includegraphics[width=.7\linewidth]{figs/graph-b.pdf}
\par\bigskip
\par\medskip
  \caption{Blossom 456 found}
  \label{fig:sfig2}
\end{subfigure}
\begin{subfigure}{.25\textwidth}
  \centering
\par\bigskip
  \includegraphics[width=.7\linewidth]{figs/graph-c.pdf}
\par\bigskip
\par\bigskip
  \caption{Blossom shrunk to 5}
  \label{fig:sfig1}
\end{subfigure}%
\begin{subfigure}{.25\textwidth}
  \centering
  \includegraphics[width=.7\linewidth]{figs/graph-d.pdf}
\par\bigskip
  \caption{No augmenting path}
  \label{fig:sfigd}
\end{subfigure}

\caption{Processing the graph \texttt{wt8}}
\label{wt8_graphs}
\end{figure}


A snapshot of this run is given in Figure \ref{wt8}. We see that 
it terminates at a return statement on line 258 at around time 1700
indicating that an augmenting path was not found.
The step count compares with
the upper bound of 8123 steps calculated above.   

%\begin{figure}[!ht]
\begin{figure}
  \centering
    \includegraphics[width=0.8\textwidth]{figs/wt8.pdf}
\caption{\texttt{wt8}: no augmenting path}
\label{wt8}
\end{figure}

In Figure \ref{wt8_graphs}(b) we see how the algorithm first finds a blossom
on vertices 4,5,6 and shrinks it to vertex 5 
as in Figure \ref{wt8_graphs}(c). In the subsequent iteration no blossom
or augmenting path is found, as shown in 
Figure \ref{wt8_graphs}(d), to the run terminates.

The input  \texttt{wt8a} contains the additional edge 47. As before a blossom on vertices
4,5,6 is found and shrunk to vertex 5. However this time an augmenting path 57 is 
found in the shrunk graph. This expands to the augmenting path 6,5,4,7 in \texttt{wt8a}.
Observing the snapshot in Figure \ref{fig:wt8a} we see that 
the run halts on line 152 of the code after about 1480 steps and returns $w=1$.

\begin{figure}
%\begin{figure}[!ht]
  \centering
    \includegraphics[width=0.8\textwidth]{figs/wt8a.pdf}
\caption{\texttt{wt8a}: augmenting path found}
\label{fig:wt8a}
\end{figure}


\begin{table}[htb]
\centering
\scalebox{0.9}{
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline

name &n &max steps&main.LB&init.UB&rows&columns&non-zeros&GB\\
\hline
\texttt{mm8.lp}& 8 &4000 (9747) & 307 & 393  &  21,490,809  &2,567,920& 80,568,489   &1.4 (3.4)   \\
\texttt{mm10.lp}&10& 7000 (19629)  &472 & 611   & 54,809,388 & 5,354,967  &  210,572,706   & 3.6 (11)\\
\texttt{mm12.lp} &12 & 10000 (34771) &673 & 877   &94,860,776&8,200,011  & 371,213,800 &6.3 (23) \\
\texttt{mm16.lp} &16 & 16000 (83003) &1183 & 1553  &  212,451,096  & 14,288,092 & 854,715,828  & 15 (80) \\
\hline
\end{tabular}
}
\caption{Linear programs generated for maximum matching}
\label{tab:mm}
\end{table}

\begin{table}[htb]
\begin{minipage}[b]{0.64\textwidth}
\centering
\scalebox{0.99}{
\begin{tabular}{|c|c|c|c||c||c||c|c|}
\hline
\multicolumn{4}{|c||}{Inputs} &\multicolumn{1}{c||}{\glpsol}&\multicolumn{1}{c||}{\cplex} &\multicolumn{2}{c|}{Outputs} \\
name & n & m    & M &  secs   & secs& answer & steps \\
\hline
\texttt{wt8.in}& 8& 15  &  3  & 32  & 27   & max & 1692   \\
\texttt{wt8a.in}& 8 & 16 & 3      &  37   & 26   & aug & 1474\\
\hline
\texttt{wt10.in}& 10 &19& 4   &98    & 240  &max  & 1627 \\
\texttt{tr10.in}& 10 & 14  &4    & 97   & 272 & aug & 3733  \\
\hline
\texttt{wt12.in}& 12 & 24  & 5   &188& 319  &max  & 2671 \\
\texttt{tr12.in}& 12 & 17  &5    & 189  & 420 & aug & 5295  \\
\hline
\texttt{wt16.in}& 16 & 43  & 7  & - & 1353   &max     & 4241 \\
\texttt{tr16.in}& 16 & 23  & 7  & -    &1956   &aug    & 9211 \\
\hline
\end{tabular}
}
\caption{Maximum matching test results}
\label{tab:mm2}
\end{minipage}\hfill
\begin{minipage}[b]{0.34\textwidth}
\centering
    \includegraphics[width=0.6\textwidth]{figs/trn.pdf}
\captionof{figure}{\texttt{trn.in}}
\label{trn}
\end{minipage}
\end{table}


Table \ref{tab:mm} shows some linear programs built for the maximum matching problem.
These are much bigger LPs than those generated for the makespan problem and the analysis
of the worst case run time given above is quite loose. To make it easier for
the solvers we generated LPs with
a time bound somewhat less than the proven worst case. 
The time bounds used are shown in column 3 with the worst case bound in parenthesis.
The phase bounds in columns 4 and 5 are as given above and the corresponding
statistics of the LP generated are given in the next 3 columns. The disk
space required to store the LP is given in the final column with the size of
the LP for the proven time bound in parentheses.

Table \ref{tab:mm2} shows the results of solving the LPs for some given input 
graphs\footnote{All runs on \texttt{mai32ef}: 4x Opteron 6376 (16-core 2.3GHz), 64 cores, 256GB memory, 4TB hard drive}.
Here $n$ denotes the number of vertices, $m$ the number of edges and $M$
the size of the given matching to test for being of maximum size.
The graphs \texttt{wtn.in} derive from Tutte's theorem and have no maximum matching,
so no augmenting path is found.
The graphs \texttt{trn.in} shown in Figure \ref{trn} achieve the maximum
number, $(n-2)/2$ of shrinkings, successively matching edges (1,2),(3,4),...,$n-3,n-4$,
before finding an augmenting path from vertex 0 to vertex $n-1$.
We observe that the steps used in finding the solution, shown in the final column,
are significantly smaller than the time bounds given in column 3 of Table \ref{tab:mm}.
\glpsol~has a limit of $10^8$ constraints and so was not able to handle
\texttt{mm16.lp}. Both \cplex~and \gurobi~ solved all problems using the presolver only.
\gurobi~was not able to solve any of the models.


%\bibliographystyle{plain}
\bibliography{mm}

\newpage

\begin{appendices}

\section{\texttt{mm.spk} : sparks code}
\label{spkcode}
\lstset{basicstyle=\small}
\begin{lstlisting}[caption={Phase \texttt{init}}]
phase init do
input matrix a[$n$,$n$]
output bool w

triangular matrix A[$n$,$n$]   # adjacency matrix
array  odd[$n$]
array  marked[$n$]    # one for marked vertices
array  F[$n$]         # one for vertices in forest F
array shrunk[$n$]     # shrunk[v]=1 for shrunk, ie. dead, vertex
int array match[$n$]  # match[v]=v for unmatched vertex
int array parent[$n$] # parent[v]=v for root
bool  x,progress,swap,edge,doneW,doneV,tip
int i,j,k,V,W,X,Y,row,col

A[*,*]<-0; shrunk[*]<-0       
for i<-0,$n-1$ do      
       match[[i]]<-i     # denotes unmatched edge
done
for i<-0,$n-2$ do      # allow for an input matching
       for j<-inc(i),$n-1$ do
          A[i,j]<-a[i,j]   # can be deleted if using greedy matching
          if a[j,i] then   # matching edge: j  i
             match[[i]] <- j; match[[j]] <- i
          endif
       done
done
progress<-1
done    # phase init finsished
\end{lstlisting}

\pagebreak
\begin{lstlisting}[caption={Phase \texttt{main}}]
phase main do

while progress do      # find aug path and exit of find blossom and  shrink
    odd[*]<-0;  marked[*]<-0 
    for i<-0,$n-1$ do      # reinitialize              
       if shrunk[i] then marked[i]<-1
       else              # only reinitialize live vertices
           parent[[i]]<-i
           F[i]<- i = match[[i]]   #unmatched alive vertices initialize F
       endif
    done
  progress<-0; V<-0; doneV<-0
  while !progress and !doneV do         # unexplored vertex
      x<-!marked[V] and !odd[V]
      if x and  F[V] then  # unexplored edge
        marked[V]<-1; W<-0; doneW<-0
        while !progress and !doneW do
         if V!= W and !shrunk[W] then   # W is still alive      
             if V < W then edge<-A[V,W]
             else edge<-A[W,V] endif
            if edge and !odd[W] then #unmarked edge VW
             if  !F[W] then          # W not in F
               X<-match[[W]]         # exposed nodes all in F
               parent[[W]]<-V        # add W and X to F
               parent[[X]]<-W        # add W and X to F
               F[W]<-1; F[X]<-1;
               odd[W]<-1;  odd[X]<-0
             else               # W path or blossom found
             #
             #see next figure for code
             #
               progress<-1   # iteration over, we got path or blossom
             endif           # W not in F
            endif            # unmarked edge VW
         endif             # if V!=W
         if W = $n-1$ then doneW<-1
         else W++  endif
      done            # while !progress and !doneW
     endif            # unexplored edge
     if V = $n-1$ then doneV<-1
     else V++ endif
  done            # while !progress and !doneV do
done              # while progress
return w @ 0      # no augmenting path 
done              # phase main
\end{lstlisting}

\pagebreak
\begin{lstlisting}[caption={Code block for path or blossom found}]
i<-V   # find roots for V and W
while i != parent[[i]] do
 i<- parent[[i]]
done
 k<-W
while k != parent[[k]] do
   k<- parent[[k]]
done
    if  i != k then      # alternating path
       return w @ 1      # success !
    else                # shrink blossom
       X<-parent[[V]]    #reverse tree edges
       parent[[V]]<-W 
       while V != X  do #reverse edges from W to root
         Y<-parent[[X]]
         parent[[X]]<-V    #reverse edge
         V<-X; X<-Y
      done      # end reverse tree edges
      V<-match[[W]]; tip<-0
      while V != W do  #traverse and shrink cycle
         shrunk[V]<-1
         if !tip then
            if !odd[V] and parent[[V]] != match[[V]] then
                tip<-1
                if match[[V]]=V then match[[W]]<-W  #V is the tip
                else match[[W]]<-match[[V]] endif
            endif
          endif
          j<-0; swap<-0; col<-W
          while j<V do       # shrink V to W
             if W=j then swap<-1;  row<-W
             else   
                if swap then col<-j
                else row<-j endif
                A[row,col]<-A[row,col] or A[j,V]  
             endif                     # copy to shrunk vertex
            j++
          done    # note: j=V, no need to reset swap
          while j != $n-1$ do
            j++           # increment here to skip j=V
             if W=j then swap<-1; row<-W
             else   
                if swap then col<-j else row<-j endif
                A[row,col]<-A[row,col] or A[V,j] 
             endif                       # copy to shrunk vertex
          done
          V<-parent[[V]]
       done          # traverse and shrink cycle
    endif      # if i != k alternating path (or blossom)
progress<-1     # iteration over, we got path or blossom
\end{lstlisting}

\section{Sample inputs for maximum matching}
\label{inputs}

\begin{verbatim}
wt8.in:
# a is an n by n binary matrix
# the upper triangle contains the adj matrix of a graph
# the lower triangle contains a (partial) matching 10, 32, 54 

matrix a[8,8] <- {{0,1,1,1,0,1,1,1},
                 {1,0,1,0,0,0,0,0},
                 {0,0,0,1,1,1,1,1},
                 {0,0,1,0,0,0,0,0},
                 {0,0,0,0,0,1,1,0},
                 {0,0,0,0,1,0,1,0},
                 {0,0,0,0,0,0,0,0},
                 {0,0,0,0,0,0,0,0}}
\end{verbatim}

\begin{verbatim}
wt8a.in:

matrix a[8,8] <- {{0,1,1,1,0,1,1,1},
                 {1,0,1,0,0,0,0,0},
                 {0,0,0,1,1,1,1,1},
                 {0,0,1,0,0,0,0,0},
                 {0,0,0,0,0,1,1,1},
                 {0,0,0,0,1,0,1,0},
                 {0,0,0,0,0,0,0,0},
                 {0,0,0,0,0,0,0,0}}
\end{verbatim}

\section{Assembly code \texttt{mm.asm}}
\lstinputlisting[language=asm,frame=none,numbers=left]{mm-minimal.asm}
\end{appendices}
\end{document}
