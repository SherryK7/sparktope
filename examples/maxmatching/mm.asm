     1	@ phase start init
     2	@ input matrix a n n	# L13
     3	@ var triangular A n n	# L14
     4	@ var array odd n	# L15
     5	@ var array marked n	# L16
     6	@ var array F n	# L17
     7	@ var array shrunk n	# L18
     8	@ var int_array match n	# L20
     9	@ var int_array parent n	# L21
    10	@ output bool w	# L22
    11	@ var bool x	# L24
    12	@ var bool y	# L25
    13	@ var bool z	# L26
    14	@ var bool progress	# L27
    15	@ var bool swap	# L28
    16	@ var bool edge	# L29
    17	@ var bool doneW	# L30
    18	@ var bool doneV	# L31
    19	@ var bool tip	# L32
    20	@ var int i	# L34
    21	@ var int j	# L35
    22	@ var int k	# L36
    23	@ var int V	# L37
    24	@ var int W	# L38
    25	@ var int X	# L39
    26	@ var int Y	# L40
    27	@ var int row	# L41
    28	@ var int col	# L42
    29	. matrix_init A 0	# L44
    30	. array_init shrunk 0	# L45
    31	. set i copyw 0	# L46
    32	. set _sentinel3 copyw $n-1$	# L46
    33	for3 row_set match i i	# L47
    34	. set _test3 eqw i _sentinel3	# L46
    35	. if _test3 done3	# L46
    36	. set i incw i	# L46
    37	. goto for3	# L46
    38	done3 set i copyw 0	# L50
    39	. set _sentinel5 copyw $n-2$	# L50
    40	for5 set j incw i	# L51
    41	. set _sentinel6 copyw $n-1$	# L51
    42	for6 set  _temp8 matrix_ref a i j	# L52
    43	. matrix_set A i j _temp8	# L52
    44	. set  _temp10 matrix_ref a j i	# L53
    45	. set guard9 copy _temp10	# L53
    46	. unless guard9 else9	# L53
    47	9 row_set match i j	# L54
    48	. row_set match j i	# L55
    49	else9 nop	# L53
    50	. set _test6 eqw j _sentinel6	# L51
    51	. if _test6 done6	# L51
    52	. set j incw j	# L51
    53	. goto for6	# L51
    54	done6 nop	# L51
    55	. set _test5 eqw i _sentinel5	# L50
    56	. if _test5 done5	# L50
    57	. set i incw i	# L50
    58	. goto for5	# L50
    59	done5 set progress copy 1	# L60
    60	@ phase end init
    61	@ phase start main
    62	14 set _test15 copy progress	# L67
    63	. unless _test15 done15	# L67
    64	16 array_init odd 0	# L68
    65	. array_init marked 0	# L69
    66	. set i copyw 0	# L70
    67	. set _sentinel18 copyw $n-1$	# L70
    68	for18 set  _temp20 array_ref shrunk i	# L71
    69	. set guard19 copy _temp20	# L71
    70	. unless guard19 else19	# L71
    71	19 array_set marked i 1	# L72
    72	. goto done19	# L71
    73	else19 row_set parent i i	# L74
    74	. set  _temp24 row_ref match i	# L75
    75	. set _temp25 eqw i _temp24	# L75
    76	. array_set F i _temp25	# L75
    77	done19 nop	# L71
    78	. set _test18 eqw i _sentinel18	# L70
    79	. if _test18 done18	# L70
    80	. set i incw i	# L70
    81	. goto for18	# L70
    82	done18 set progress copy 0	# L80
    83	. set V copyw 0	# L81
    84	. set doneV copy 0	# L82
    85	while29 set  _temp30 not progress	# L83
    86	. set  _temp31 not doneV	# L83
    87	. set _test29 and _temp30 _temp31	# L83
    88	. unless _test29 done29	# L83
    89	30 set  _temp33 array_ref marked V	# L84
    90	. set  _temp34 not _temp33	# L84
    91	. set  _temp35 array_ref odd V	# L84
    92	. set  _temp36 not _temp35	# L84
    93	. set x and _temp34 _temp36	# L84
    94	. set  _temp38 array_ref F V	# L85
    95	. set guard37 and x _temp38	# L85
    96	. unless guard37 else37	# L85
    97	37 array_set marked V 1	# L86
    98	. set W copyw 0	# L87
    99	. set doneW copy 0	# L88
   100	while42 set  _temp43 not progress	# L89
   101	. set  _temp44 not doneW	# L89
   102	. set _test42 and _temp43 _temp44	# L89
   103	. unless _test42 done42	# L89
   104	43 set _temp48 eqw V W	# L90
   105	. set _temp46 not _temp48	# L90
   106	. set  _temp49 array_ref shrunk W	# L90
   107	. set _temp47 not _temp49	# L90
   108	. set guard45 and _temp46 _temp47	# L90
   109	. unless guard45 else45	# L90
   110	45 set guard50 ltw V W	# L91
   111	. unless guard50 else50	# L91
   112	50 set  _temp52 matrix_ref A V W	# L92
   113	. set edge copy _temp52	# L92
   114	. goto done50	# L91
   115	else50 set  _temp54 matrix_ref A W V	# L94
   116	. set edge copy _temp54	# L94
   117	done50 set  _temp56 array_ref odd W	# L96
   118	. set  _temp57 not _temp56	# L96
   119	. set guard55 and edge _temp57	# L96
   120	. unless guard55 else55	# L96
   121	55 set  _temp59 array_ref F W	# L97
   122	. set guard58 not _temp59	# L97
   123	. unless guard58 else58	# L97
   124	58 set  _temp61 row_ref match W	# L98
   125	. set X copyw _temp61	# L98
   126	. row_set parent W V	# L99
   127	. row_set parent X W	# L100
   128	. array_set F W 1	# L101
   129	. array_set F X 1	# L102
   130	. array_set odd W 1	# L103
   131	. array_set odd X 0	# L104
   132	. goto done58	# L97
   133	else58 set i copyw V	# L106
   134	while69 set  _temp71 row_ref parent i	# L107
   135	. set _temp70 eqw i _temp71	# L107
   136	. set _test69 not _temp70	# L107
   137	. unless _test69 done69	# L107
   138	70 set  _temp73 row_ref parent i	# L108
   139	. set i copyw _temp73	# L108
   140	. goto while69	# L107
   141	done69 set k copyw W	# L110
   142	while75 set  _temp77 row_ref parent k	# L111
   143	. set _temp76 eqw k _temp77	# L111
   144	. set _test75 not _temp76	# L111
   145	. unless _test75 done75	# L111
   146	76 set  _temp79 row_ref parent k	# L112
   147	. set k copyw _temp79	# L112
   148	. goto while75	# L111
   149	done75 set _temp81 eqw i k	# L114
   150	. set guard80 not _temp81	# L114
   151	. unless guard80 else80	# L114
   152	80 return w copy 1	# L115
   153	. goto done80	# L114
   154	else80 set  _temp84 row_ref parent V	# L117
   155	. set X copyw _temp84	# L117
   156	. row_set parent V W	# L118
   157	while86 set _temp87 eqw V X	# L119
   158	. set _test86 not _temp87	# L119
   159	. unless _test86 done86	# L119
   160	87 set  _temp89 row_ref parent X	# L120
   161	. set Y copyw _temp89	# L120
   162	. row_set parent X V	# L121
   163	. set V copyw X	# L122
   164	. set X copyw Y	# L123
   165	. goto while86	# L119
   166	done86 set  _temp94 row_ref match W	# L125
   167	. set V copyw _temp94	# L125
   168	. set tip copy 0	# L126
   169	while96 set _temp97 eqw V W	# L127
   170	. set _test96 not _temp97	# L127
   171	. unless _test96 done96	# L127
   172	97 array_set shrunk V 1	# L128
   173	. set guard99 not tip	# L129
   174	. unless guard99 else99	# L129
   175	99 set  _temp103 array_ref odd V	# L130
   176	. set _temp101 not _temp103	# L130
   177	. set  _temp105 row_ref parent V	# L130
   178	. set  _temp106 row_ref match V	# L130
   179	. set _temp104 eqw _temp105 _temp106	# L130
   180	. set _temp102 not _temp104	# L130
   181	. set guard100 and _temp101 _temp102	# L130
   182	. unless guard100 else100	# L130
   183	100 set tip copy 1	# L131
   184	. set  _temp109 row_ref match V	# L132
   185	. set guard108 eqw _temp109 V	# L132
   186	. unless guard108 else108	# L132
   187	108 row_set match W W	# L133
   188	. goto done108	# L132
   189	else108 set  _temp112 row_ref match V	# L135
   190	. row_set match W _temp112	# L135
   191	done108 nop	# L132
   192	else100 nop	# L130
   193	else99 set j copyw 0	# L139
   194	. set swap copy 0	# L140
   195	. set col copyw W	# L141
   196	while116 set _test116 ltw j V	# L142
   197	. unless _test116 done116	# L142
   198	117 set guard117 eqw W j	# L143
   199	. unless guard117 else117	# L143
   200	117 set swap copy 1	# L144
   201	. set row copyw W	# L145
   202	. goto done117	# L143
   203	else117 set guard120 copy swap	# L147
   204	. unless guard120 else120	# L147
   205	120 set col copyw j	# L148
   206	. goto done120	# L147
   207	else120 set row copyw j	# L150
   208	done120 set  _temp124 matrix_ref A row col	# L152
   209	. set  _temp125 matrix_ref A j V	# L152
   210	. set _temp126 or _temp124 _temp125	# L152
   211	. matrix_set A row col _temp126	# L152
   212	done117 set j incw j	# L154
   213	. goto while116	# L142
   214	done116 set _temp129 eqw j $n-1$	# L157
   215	. set _test128 not _temp129	# L157
   216	. unless _test128 done128	# L157
   217	129 set j incw j	# L158
   218	. set guard131 eqw W j	# L159
   219	. unless guard131 else131	# L159
   220	131 set swap copy 1	# L160
   221	. set row copyw W	# L161
   222	. goto done131	# L159
   223	else131 set guard134 copy swap	# L163
   224	. unless guard134 else134	# L163
   225	134 set col copyw j	# L164
   226	. goto done134	# L163
   227	else134 set row copyw j	# L166
   228	done134 set  _temp138 matrix_ref A row col	# L168
   229	. set  _temp139 matrix_ref A V j	# L168
   230	. set _temp140 or _temp138 _temp139	# L168
   231	. matrix_set A row col _temp140	# L168
   232	done131 nop	# L159
   233	. goto done116	# L157
   234	done128 set  _temp142 row_ref parent V	# L171
   235	. set V copyw _temp142	# L171
   236	. goto while96	# L127
   237	done96 nop	# L127
   238	done80 set progress copy 1	# L174
   239	done58 nop	# L97
   240	else55 nop	# L96
   241	else45 set guard144 eqw W $n-1$	# L178
   242	. unless guard144 else144	# L178
   243	144 set doneW copy 1	# L179
   244	. goto done144	# L178
   245	else144 set W incw W	# L181
   246	done144 nop	# L178
   247	. goto while42	# L89
   248	done42 nop	# L89
   249	else37 set guard147 eqw V $n-1$	# L185
   250	. unless guard147 else147	# L185
   251	147 set doneV copy 1	# L186
   252	. goto done147	# L185
   253	else147 set V incw V	# L188
   254	done147 nop	# L185
   255	. goto while29	# L83
   256	done29 nop	# L83
   257	. goto 14	# L67
   258	done15 return w copy 0	# L193
   259	@ phase end main
   260	. nop	# L65
