#!/usr/bin/env pypy
# -*- python -*-
import argparse
import json
import sys
import os.path

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../lib/python")))

from linprog.constraintset import ConstraintSet
from linprog.LPVariable import LPVariable

parser = argparse.ArgumentParser(description="Convert JSON file to objective function. This file should define maps with keys 'c' for inputs and 'd' for outputs")

parser.add_argument('--output', action='store',
                    default='lp',
                    help='lp|gmpl|none')

parser.add_argument('--force', action='store_true',
                    default=False,
                    help='force input variables')

parser.add_argument('infile', nargs='?', type=argparse.FileType('r'),
                    default=sys.stdin)

args = parser.parse_args()

with args.infile as f:
    dat = json.load(f)

linearities=ConstraintSet(file=sys.stdout,output=args.output)

terms=[(-1,LPVariable('zzobj','scalar'))]
for param in dat:
    for key in dat[param]:
        v = dat[param][key]
        terms.append((v,LPVariable.from_string(key)))
        
linearities.append("OBJDEF",0,' = ',*terms)

if args.force:
    if 'c' in dat:
        for key in dat['c']:
            forced_val=None
            v = dat['c'][key]
            if v<0:
                forced_val=0
            elif v>0:
                forced_val=1
            if forced_val != None:
                var = LPVariable.from_string(key)
                linearities.append('force_{k:s}'.format(k=var.stringify(args.output)),
                                   forced_val,' = ',var)
