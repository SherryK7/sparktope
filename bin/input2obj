#!/usr/bin/env racket
#lang racket/base
(require racket/cmdline)
(require racket/hash)
(require racket/list)
(require syntax/parse)
(require "../lib/racket/input-grammar.rkt")
(require "../lib/racket/lexer.rkt")

(define word-size (make-parameter #f))
(define param-file (make-parameter #f))
(define output-weight (make-parameter 0))

(define (compile-file stx)
  (syntax-parse stx
    [({~literal defns} defns ...)
     (for/fold
         ([in-acc (hash)]
          [out-acc (hash)]
          #:result (hash  'c in-acc  'd out-acc))
         ([defn (syntax->list #'(defns ...))])
       (values
        (hash-union in-acc (compile-defn defn))
        (hash-union out-acc (compile-output defn))))]))

(define (compile-output stx)
  (syntax-parse stx
    #:datum-literals (output)
    [(output _ "bool" ident) (hash (sym (syntax-e #'ident)) (output-weight))]
    [(output _ "bool" ident "*" coeff:number) (hash (sym (syntax-e #'ident)) (syntax-e #'coeff))]
    [(output _ "bool" ident "*" numer:number "/" denom:number) (hash (sym (syntax-e #'ident))
                                                                     (list (syntax-e #'numer) (syntax-e #'denom)))]
    [(output _ "int" stuff ...) (raise-user-error 'compile-output "integer outputs currently unsupported")]
    [else  (hash)]))

(define (apply/unstx proc . args)
  (apply proc (map (lambda (arg) (if (syntax? arg) (syntax->datum arg) arg))
                   (apply list* args))))

(define (compile-defn defn)
  (define-syntax-class bool (pattern (~or 0 1)))
  (define (compile-alit stx)
    (syntax-parse stx
      [({~literal array-literal} "{" term0:bool (~seq "," term:bool) ... "}")
       (syntax->datum #'(term0 term ...))]))
  (syntax-parse defn
    #:datum-literals (array scalar matrix int-array output)
    [(output stuff ...) (hash)]
    [(array _ ident "[" size "]" "<-"  alit)
     (apply/unstx array #'ident #'size (compile-alit #'alit))]
    [(matrix _ ident "[" nrows "," ncols "]" "<-" "{" row0 (~seq "," rows) ... "}")
     (apply/unstx matrix #'ident #'nrows #'ncols (map compile-alit (syntax->list #'(row0 rows ...))))]
    [(int-array _ _ ident "[" size "]" "<-" (array-literal "{" num0:number (~seq "," nums:number) ... "}"))
     (apply int-array (syntax->datum #'(ident size num0 nums ...)))]
    [(scalar "bool" ident "<-" val:bool) (hash  (sym (syntax-e #'ident)) (coeff (syntax-e #'val)))]
    [(scalar "int" ident "<-" val:integer) (apply int (syntax->datum #'(ident val)))]))

(define (sym fmt . args)
  (string->symbol (apply format fmt args)))

(define (coeff v)
  (if (zero? v) -2 2))

(define (int ident val)
  (unless (> (expt 2 (word-size)) val)
    (raise-user-error 'int "number ~a too big for word size ~a" val (word-size)))
  (for/hash
      ([i (in-range (word-size))]
       [v (bits val (word-size))])
    (values (sym "~a[~a]" ident i) (coeff v))))

(define (matrix ident nrows ncols . rows)
  (let ([row-count (length rows)]
        [col-count (length (first rows))])
    (unless (= nrows row-count)
      (raise-user-error 'array "row count ~a != ~a" row-count nrows))
    (unless (= ncols col-count)
      (raise-user-error 'array "col count ~a != ~a" col-count ncols)))
  (for/fold
      ([acc (hash)])
      ([row rows] [i (in-range nrows)])
    (hash-union
     acc
     (for/hash
         ([j (in-range ncols)]
          [b row])
       (values (sym "~a[~a,~a]" ident i j) (coeff b))))))

(define (array ident size . elements)
  (let ([elt-size (length elements)])
    (unless (= size elt-size) (raise-user-error 'array "size mismatch ~a ~a" size elt-size)))
  (for/hash
      ([i (in-range size)]
       [v elements])
    (values (sym "~a[~a]" ident i) (coeff v))))

(define (int-array ident size . elements)
  (let ([elt-size (length elements)])
    (unless (= size elt-size) (raise-user-error 'int-array "size mismatch ~a ~a" size elt-size)))
  (apply matrix ident size (word-size)
          (map (lambda (v) (bits v (word-size))) elements)))

(define (bits int-val width)
    (if (zero? width)
        '()
        (let-values ([(q r) (quotient/remainder int-val 2)])
          (cons r (bits q (sub1 width))))))

(define (read-param-file file-name)
  (with-input-from-file file-name
    (λ ()
      (for ([line (in-lines)])
        (let ([match (regexp-match #px"\\s*word_size\\s*=\\s*(\\d+)" line)])
          (when match
            (word-size (string->number (second match))))))))
  (unless (word-size)
    (raise-user-error 'read-param-file "no parameter word_size in ~a" file-name)))

(module+ main
  (require json)
  (command-line
   #:program "input2obj"
   #:once-each
   [("-w" "--word-size") ws "number of bits in integers"
                         (word-size (string->number ws))]
   [("-p" "--param-file") pf "param file"
                         (param-file pf)]

   #:ps
   "\nSample Input:"
   " bool x <- 0"
   " int m <- 3"
   " array A[3] <- { 1, 0, 1}"
   " matrix M[3,3] <- { {0,0,0}, { 0, 1, 1}, {1,0,1}}"
   " int array I[3] <- { 1, 2, 3}"
   " output bool w"
   "\n Sample Session:"
   " % input2obj -p foo.param foo.in > foo.jso"
   " % runpoly foo.lp foo.jso > foo.out"

   #:args ([in-file #f])
   (let ([in-port
          (if in-file
              (open-input-file in-file)
              (current-input-port))])
     (when (param-file)
       (read-param-file (param-file)))
     (unless (word-size)
       (raise-user-error 'input2obj "Please specify --word-size or --param-file"))
     (write-json (compile-file (parse (tokenize in-port)))))))

(module+ test
  (require "../lib/racket/test-utils.rkt")
  (require rackunit)
  (define (compile-string str)
    (compile-file (parse (tokenize-string str))))
  (define (input-hash str)
    (hash-ref (compile-string str) 'c))
  (define (output-hash str)
    (hash-ref (compile-string str) 'd))

  (define-fail-check check-error? compile-string)
  (define-parse-check check-parse? parse)

  (parameterize ([word-size 3])
    (check-parse? "int n <- 3" '(defns (scalar "int" "n" "<-" 3)))
    (check-parse? "output bool w" '(defns (output "output" "bool" "w")))
    (check-parse? "output bool w * 1" '(defns (output "output" "bool" "w" "*" 1)))
    (check-parse? "output bool w * 1/3" '(defns (output "output" "bool" "w" "*" 1 "/" 3)))
    (check-parse? "bool x <- 1" '(defns (scalar "bool" "x" "<-" 1)))
    (check-parse? "array A[3] <- { 0, 1, 1}"
                  '(defns
                     (array "array" "A" "[" 3 "]" "<-" (array-literal "{" 0 "," 1 "," 1 "}"))))
    (check-parse? "matrix M[3,3] <- { {0,0,0}, { 0, 1, 1}, {1,0,1}}"
                  '(defns (matrix "matrix" "M" "[" 3 "," 3 "]" "<-" "{"
                                  (array-literal "{" 0 "," 0 "," 0 "}") ","
                                  (array-literal "{" 0 "," 1 "," 1 "}") ","
                                  (array-literal "{" 1 "," 0 "," 1 "}") "}")))
    (check-parse? "int array A[3] <- { 1, 2, 3}"
                  '(defns
                     (int-array "int" "array" "A" "[" 3 "]" "<-" (array-literal "{" 1 "," 2 "," 3 "}"))))
    (check-equal? (input-hash "int n <- 3") #hash((|n[0]| . 2) (|n[1]| . 2) (|n[2]| . -2)))
    (check-equal? (output-hash "int n <- 3") #hash())
    (check-equal? (output-hash "output bool w") #hash((w . 0)))
    (check-equal? (compile-string  "bool x <-1
                                    output bool w")
                  #hash((d . #hash((w . 0)))
                        (c . #hash((x . 2)))))
        (check-equal? (input-hash "array A[3] <- { 1, 0, 1}") #hash((|A[0]| . 2) (|A[1]| . -2) (|A[2]| . 2)))
    (check-error? "output int w" #rx"unsupported")
    (check-equal? (input-hash "matrix M[3,3] <- { {0,0,0}, { 0, 1, 1}, {1,0,1}}")
                  #hash((|M[0,0]| . -2) (|M[0,1]| . -2) (|M[0,2]| . -2)
                                        (|M[1,0]| . -2) (|M[1,1]| . 2) (|M[1,2]| . 2)
                                        (|M[2,0]| . 2)  (|M[2,1]| . -2) (|M[2,2]| . 2)))
    (check-equal? (input-hash "int array I[3] <- { 1, 2, 3}")
                  #hash((|I[0,0]| . 2) (|I[0,1]| . -2) (|I[0,2]| . -2)
                                       (|I[1,0]| . -2) (|I[1,1]| . 2) (|I[1,2]| . -2)
                                       (|I[2,0]| . 2) (|I[2,1]| . 2) (|I[2,2]| . -2)))
    (check-equal? (output-hash "output bool w") #hash((w . 0)))
    (check-equal? (output-hash "output bool w * 1") #hash((w . 1)))
    (check-equal? (output-hash "output bool w * 1/3") #hash((w . (1 3))))
    (check-equal? (input-hash "bool x <- 0
                                 int m <- 3
                                 array A[3] <- { 1, 0, 1}
                                 matrix M[3,3] <- { {0,0,0}, { 0, 1, 1}, {1,0,1}}
                                 int array I[3] <- { 1, 2, 3}")
                  #hash((|x| . -2)
                        (|m[0]| . 2) (|m[1]| . 2) (|m[2]| . -2)
                        (|M[0,0]| . -2) (|M[0,1]| . -2) (|M[0,2]| . -2)
                        (|M[1,0]| . -2) (|M[1,1]| . 2) (|M[1,2]| . 2)
                        (|M[2,0]| . 2)  (|M[2,1]| . -2) (|M[2,2]| . 2)
                        (|A[0]| . 2) (|A[1]| . -2) (|A[2]| . 2)
                        (|I[0,0]| . 2) (|I[0,1]| . -2) (|I[0,2]| . -2)
                        (|I[1,0]| . -2) (|I[1,1]| . 2) (|I[1,2]| . -2)
                        (|I[2,0]| . 2) (|I[2,1]| . 2) (|I[2,2]| . -2)))))
