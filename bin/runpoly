#!/usr/bin/env bash
# -*- shell-script -*-

export PATH=$PATH:$(dirname $(readlink -f "$0"))

mode='glp'
extra_args=''
force_args=''
digits_args=''
debug=0

cleanup() {
    if [ x$debug = x0 ]; then
	rm -f $log
	rm -f $xml
	rm -f $mergedlp
	rm -f $gursol
    fi
}

trap cleanup EXIT

usage () {
    cat <<EOF
usage $0  [options] lp-file objective-file

Options:

	-d, --debug			Preserve intermediate files.
	-f --force			Force input values
	-l, --lp			Use glpsol with LP format (default)
	-g, --gmpl			Use glpsol with GMPL format
	-c, --cplex			Use CPLEX
	-u, --gurobi			Use Gurobi
	-x, --xcheck			Tell glpsol to verify the solution with
	    				exact arithmetic.
        -X  --exact                     Tell glpsol to use exact arithmetic
        -h, --help			Display this help

EOF
}

while /bin/true; do
    case $1 in
	-d|--debug)
	    debug=1
	    shift
	    ;;
	-f|--force)
	    force_args="--force"
	    shift
	    ;;
	-x|--xcheck)
	    glp_args="--xcheck"
	    shift
	    ;;
	-X|--exact)
	    glp_args="--exact"
	    shift
	    ;;
	-D|--digits)
	    digits_args="--digits $2"
	    shift
	    shift
	    ;;
	-g|--gmpl)
	    mode='gmpl'
	    shift
	    ;;
	-l|--glp)
	    mode='glp'
	    shift
	    ;;
	-c|--cplex|--clp)
	    mode='clp'
	    shift
	    ;;
	-u|--gurobi|--gur)
	    mode='gur'
	    shift
	    ;;
	-h|--help)
	    usage
	    exit 0
	    ;;
	-*)
	    echo "unrecognized option $1"
	    exit 1
	    ;;
	*)
	    break
    esac
done

if (($# != 2)); then
    usage
    exit 1
fi

polyhedron=$1
objective=$2

pbase=$(basename ${polyhedron})
obase=$(basename ${objective})
log=$(mktemp ${pbase}-XXXXX.log)
xml=$(mktemp ${pbase}-${obase}-XXXXX.xml)
mergedlp=$(mktemp ${pbase}-${obase}-XXXXX.lp)
gursol=$(mktemp ${pbase}-${obase}-XXXXX.sol)

case $mode in
    gmpl)
	glpsol $glp_args -m $1 -d $2 --log $log -o >(glpsol2var) >/dev/null
	;;
    glp)
	glpsol $glp_args --lp <(mergeobj $force_args $polyhedron $objective) -o >(glpsol2var) >/dev/null
	;;
    clp)
	mergeobj  $force_args $polyhedron $objective > $mergedlp
	rm -f $xml
	cplex -c "read $mergedlp" optimize "write $xml sol" 1>$log
	cplexsol2var $digits_args $xml
	;;
   gur)
  mergeobj $force_args $polyhedron $objective > $mergedlp
  rm -f $gursol
	gurobi_cl result_file=$gursol LogFile=$log $mergedlp >/dev/null
	gurobisol2var $digits_args $gursol
	;;
esac
