#!/usr/bin/env bash
# -*- shell-script -*-

export PATH=$PATH:$(dirname $(readlink -f "$0"))

extra_args=''
force_args=''
symbol_args=''
output_args='--output=lp'
debug=0

cleanup() {
    if [ x$debug = x0 ]; then
	rm -f $asm
    fi
}

usage () {
    cat <<EOF
usage $0 [--symbol-file file] [--force] [--lp] [--gmpl] sparks-file param-file

Generate a linear program given a sparks source and a parameter file.
This is a wrapper that invokes spk2asm (stage1) and asm2lp (stage2)

Options:

	-d, --debug			Preserve intermediate files.
	-s file, --symbol-file file	Save optional line number information
	-f --force			Force input values
	-l, --lp			Output CPLEX LP format (default)
	-g, --gmpl			Output Gnu Math Programming Language
EOF
}
trap cleanup EXIT

while /bin/true; do
    case $1 in
	-d|--debug)
	    debug=1
	    shift
	    ;;
	-s|--symbol-file)
	    symbol_args="--symbol-file $2"
	    shift
	    shift
	    ;;
	-f|--force)
	    force_args="--force-input"
	    shift
	    ;;
	-l|--lp|--glp|--clp|--gur)
	    output_args='--output=lp'
	    shift
	    ;;
	-g|--gmpl)
	    output_args='--output=gmpl'
	    shift
	    ;;
	-h|--help)
	    usage
	    exit 0
	    ;;
	--*)
	    echo "unrecognized option $1"
	    exit 1
	    ;;
	*)
	    break
	    ;;
    esac
done

sparks=$1
params=$2

sbase=$(basename ${sparks} .spk)

asm=$(mktemp ${sbase}-${obase}-XXXXX.asm)
sym=${sbase}.sym
spk2asm ${symbol_args}  ${sparks} > ${asm}

asm2lp ${force_args} ${output_args} ${asm} ${params}
