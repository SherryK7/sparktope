Installation
============

To install, just unpack somewhere convenient for you and add the bin
subdirectory to your PATH.

Prerequisites
=============

To run, you will need the following

- bash

- perl

- python 2.7. You almost certainly want pypy for anything beyond the
  tiniest examples.

- racket. Tested with 6.1.1 and 6.7
  After installing racket, you need to install ragg (parser generator)

  As root:
  
  # raco pkg install -i ragg

  or

  % raco pkg install -u ragg

- An LP solver. Currently glpsol (from glpk) and CPLEX (from
  IBM/ILOG) are supported.
 
Test suite
==========

To verify your installation, using glpsol

% cd tests && ./all-tests

or

% cd tests && ./all-tests --lp

Using cplex,

% cd tests && ./all-tests --cplex

On a recent machine this takes about 5 minutes.
