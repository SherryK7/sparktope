\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\bibliographystyle{ieeetr}
\title{Sparktope User Reference}
\date{2020-04-03}
\usepackage{fullpage}
\usepackage{listings}
\DeclareMathOperator{\steps}{steps}
\newcommand{\codevar}[1]{\textsf{#1}}
\newcommand{\Lower}{\codevar{lower}}
\newcommand{\Upper}{\codevar{upper}}
\newcommand{\Body}{\codevar{body}}
\newcommand{\BoolExpr}{\codevar{bool\_expr}}


\lstdefinelanguage{JavaScript}{
  keywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, for, in, while, do, else, case, break},
  ndkeywords={class, export, boolean, throw, implements, import, this},
  sensitive=false,
  comment=[l]{//},
  escapeinside=<>,
  mathescape=true,
  showstringspaces=false,
  morecomment=[s][\color{blue}\ttfamily]{/*}{*/},
  morestring=[b]',
  morestring=[b]"
}
\lstdefinestyle{mathy}{mathescape=true,escapebegin=}
\usepackage{sparklistings}
\usepackage{tcolorbox}
\tcbuselibrary{listings}
\newtcblisting{sparksbox}[1][]{listing only,listing remove caption=false,colback=black!3,listing options={language=sparks,frame=none,#1}}
\newcommand{\sac}[3]{%
\lstinline[language=sparks,escapechar=,mathescape=true,escapebegin=]|#1|
&
\lstinline[language=asm,escapechar=,mathescape=true,escapebegin=]|#2|
&
#3}
\newtcolorbox{spasm}{colback=black!3,colframe=black,sidebyside,lefthand width=1.6in}

\newcommand{\Sparks}{\textsc{Sparks}}
\newcommand{\Asm}{\textsc{Asm}}

\begin{document}
\maketitle
\lstset{language=sparks}
\section{Overview}

\textsc{Sparktope} is a compiler from a simple imperative language
called \textsc{Sparks} to Linear Programs (i.e. sets of linear
inequalities along with a linear objective function).  To solve a
problem using \textsc{Sparktope}, the user needs to create 3
files. First, one needs a description of the algorithm in
\Sparks{}. Conventionally named \textit{something}.spk, the syntax of
this file is described in Section~\ref{sec:sparks}. The next thing one
needs is a \emph{parameter} file describing certain important
parameters such as the word size and the input size (i.e. how many
nodes in a graph, or how many rows and columns in a matrix).  The
\Sparks{} file and the parameter file together are input to the tool
\texttt{buildpoly} which generates a polyhedron (linear program
without an objective function) that can be used to solve any problem
instance of a given size.  Finally each problem instance needs to be
described in an \emph{input file} with syntax described in
Section~\ref{sec:input}. A sample session of this entire process is
given in Section~\ref{sec:sample}.


\section{SPARKS syntax}
\label{sec:sparks}
A SPARKS program consists of declarations and statements. In principle
these can be interleaved, but putting all the declarations first is
the most tested case.

\subsection{Declarations}

Identifiers should start with a letter, and can have numbers and `\_'
in them. There is no fixed limit on identifier length.

\begin{sparksbox}
input bool x          # single bit input

input int x           # word_size defines number of bits

input array x[3]      # input array of 3 bits, indexed 0..2

input array x[n]      # input array; n is a parameter.
                      # indexed 0..n-1

input matrix A[m,n]   # input matrix (2D array of bits)
\end{sparksbox}

All of the above are valid with \textbf{input} omitted, to declare
working variables, or replaced with \textbf{output}, to declare
outputs.  Non-Boolean outputs are not tested. Multiple working
variables of the same type can be declared seperated by commas.
\begin{sparksbox}
  int x,y,z
\end{sparksbox}


\subsection{Arithmetic macros}

Anywhere an integer literal (constant) can be written, you can also
write a parameter (compile time constant defined in a parameter file)
or between \$\ldots\$, an arithmetic expression\footnote{Anything
  supported by Python should work, but we recommend keeping it simple.}.
involving integer constants and parameters.

\begin{sparksbox}[mathescape=false]
input matrix A[$n-1$,$2**2-1$]
int z
z <- $word_size+1$
\end{sparksbox}

\subsection{Statements}


The two main kinds of statements in SPARKS are assignments and control
flow. Statements in \Sparks{} can be separated by `\lstinline|;|' but
these are just to make things clearer for a human reader; the compiler
sees them as whitespace.

\subsubsection{Assignments}

\paragraph{Boolean assignments} These statements assign the result of
evaluating a Boolean expression $\beta$ to a 1 bit memory location.
\begin{sparksbox}[style=mathy]
x <- $\beta$                # assign boolean variable

x[i] <- $\beta$             # assign an array element

A[i,j] <- $\beta$           # assign a matrix (2D array) element

A{i,j} <- $\beta$           # assign A[i,j] = A[j,i] = 1
\end{sparksbox}

\paragraph{Integer assignments} There are two syntactically distinct
kinds of assignments for integers.
\begin{sparksbox}[style=mathy]
i <- $\kappa$              # assign one integer

A[[i]] <- $\kappa$         # assign one row of a matrix to an integer
\end{sparksbox}
Here $\kappa$ should be expression evaluating to an integer.

Incrementing an integer variable is written as
\begin{sparksbox}
  i++
\end{sparksbox}
This is currently equivalent to (see below for definition of \lstinline|inc|).
\begin{sparksbox}
  i <- inc(i)
\end{sparksbox}
\paragraph{Array and matrix initializations}

All entries of an array can be initialized to one compile time constant
\begin{sparksbox}[mathescape=false]
array A[3]
matrix B[n,n]

A[*]<-1
A[*]<-0

B[*,*]=$n%2$
\end{sparksbox}
\subsection{Expressions}

\subsubsection{Integer Expressions}

Only very simple integer expressions are supported, namely
\begin{itemize}
\item variables previously declared as integer
\item integer constants
\item parameters
\item row references of the form \lstinline|A[[i]]|
\end{itemize}

\paragraph{Functions}

In principle various integer functions could be supported. Currently
only \lstinline|inc| and \lstinline|dec| are supported, which allows
writing expressions containing $n+1$ and $n-1$

\begin{sparksbox}
z <- inc(n)
z <- dec(n)
\end{sparksbox}


\subsubsection{Boolean Expressions}

The following are legitimate unary Boolean expressions
\begin{sparksbox}
x
!x
a[j]                  # a is an array
m[i,j]                # b is a matrix
\end{sparksbox}

The basic binary Boolean operations are as follows:
\begin{sparksbox}
x and y
x or y
x xor y
x eq y
\end{sparksbox}
\lstinline|eq| is the Boolean version of equality (i.e. negation after xor).
In the above binary expressions, \lstinline|x| or \lstinline|y| can be replaced by their negations \lstinline|!x|, \lstinline|!y|.The only binary operations supported for integer operands are comparisons.
These are listed here because they return a Boolean.
\begin{sparksbox}
x = y
x != y
x < y
\end{sparksbox}

\subsubsection{Compound expressions}

\textsc{Sparks} supports a single level of compound expressions.  In
particular integer or Boolean expression above joined by an operator
to any other with matching type (i.e. \texttt{bool} or \texttt{int}).

\subsection{Control Flow}
\paragraph{return}  The return statement is a combination of control flow and assignment. Any supported Boolean expression is allowed after '@'
This code
\begin{sparksbox}[style=mathy]
  return w @ $\beta$
\end{sparksbox}
is roughly equivalent to
\begin{sparksbox}[style=mathy]
   w <- $\beta$
1: goto 1
\end{sparksbox}

\paragraph{If-then-else}

If-then blocks can be controlled by any supported Boolean expression
and may have a single optional else block.
\begin{sparksbox}
if y < 10 then
   return w @ 1
endif

if x then
   return w @ 1
else
   return w @ 0
endif
\end{sparksbox}

\paragraph{While loops}

While loops can be controlled by any supported Boolean expression.

\begin{sparksbox}[style=mathy]
while $\beta$ do
      i++
done
\end{sparksbox}

\paragraph{For loops}

A \lstinline|for| loop sequentially assigns a given index variable
between the given lower and upper bounds, inclusive.
\begin{sparksbox}[style=mathy]
for $\eta$ <- $\kappa_1$, $\kappa_2$ do
     nop
done
\end{sparksbox}

The expressions $\kappa_1$ and $\kappa_2$ are integer
expressions. $\eta$ should be previously declared as integer.

\subsection{Phase Blocks}

As an optimization, it is possible to tell the compiler that certain
parts of the program are only run in certain ranges of time steps. A
typical example is initialization code that is not revisited after it
runs once.  By bounding the time steps on which a given line of code
may be visited, the size of the resulting LP is reduced. Note that if
the bounds are incorrect, the computation will be silently wrong.

\begin{sparksbox}
phase init do
  # initialization part
done

phase main do
  # main part 
done
\end{sparksbox}

In a corresponding parameter file, we can specify upper and lower bounds by \texttt{UB} and \texttt{LB} respectively.
\begin{verbatim}
n=6
word_size=3
time_bound=2000
main.LB=220
init.UB=290
\end{verbatim}

\section{Parameters}
\label{sec:parameters}

When inequalities are generated by
\texttt{buildpoly}\footnote{Currently this file is used only by the
  stage 2 compiler which generates inequalities from the assembly
  code; see Section~\ref{sec:assembly} for more on this process.} ,
the %
compiler is passed a file of integer parameters
\begin{verbatim}
ident1=value1
ident2=value2
\end{verbatim}
These are substituted in early in the compilation process. They are
roughly equivalent to a \#define in C.  Note that the compiler will be
confused if you used the same name for a parameter and a variable. Several
parameters are treated specially by the compiler, in particular
\lstinline|word_size| and \lstinline|time_bound|. It is a convention
that the input size is specified by the parameter \lstinline|n|, but
this is not enforced by the compiler. An example parameter file follows.
\begin{verbatim}
n=6
word_size=3
time_bound=2000
\end{verbatim}

\section{Assembly Language}
\label{sec:assembly}

The \texttt{stage2} compiler takes a register based assembly language
for some imaginary machine, and generates constraints. It is also
possible (although probably not recommended) to write assembly
language directly. The general format of an instruction is
\begin{sparksbox}[style=mathy]
label instruction arg1 arg2 arg3 $\dots$
\end{sparksbox}
Lines that do not need an explicit label should use `\lstinline{.}'; in the following tables, the label is
omitted completely.

The \Sparks{} user might be interested in this assembly language from
two points of view. To generate a tight upper bound on the number of
steps executed by a \Sparks{} program (as a function of the input
size) it is sometimes necessary to know the expansion of \Sparks{}
statements into \Asm{} instructions. This is counting of steps is
discussed further in Section~\ref{sec:steps}.  Probably less commonly,
a user might wish to estimate the size of the set of inequalities
resulting from compiling a particular program with a particular word
size and input size. This estimation is discussed further in
Section~\ref{sec:constraints}.

\subsection{Converting Sparks to Assembly}
\label{sec:steps}
As noted above, the number of assembler code instructions is necessary to obtain
bounds on the size of the LP constraint set generated.
These bounds are given for each code sample below.
In these samples, $x,y,z$ are assumed declared boolean,
$i,j,k$ are assumed declared integer, $A$ is a boolean array, and $B$
is an integer array.
\subsubsection{Simple assignments}
\label{sec:simple}
 The following \Sparks{} statements
translate one-to-one to \Asm{} statements.
\begin{spasm}
\begin{sparks}
z <- x
z <- !x
z <- x and y
z <- x or y
z <- x xor y
z <- x eq y
z <- i = j
z <- i < j
i <- j
i <- j + k
i <- inc(j)
i <- dec(j)
i++
A[*] <- 0
B[*,*] <- 0
A[i] <- x
B[[i]] <- j
\end{sparks}
  \tcblower
\begin{asm}
. set z copy x
. set z not x
. set z and x y
. set z or x y
. set z xor x y
. set z eq x y
. set z eqw i j
. set z ltw i j
. set i copyw j
. set i addw j k
. set i incw j
. set i decw j
. set i incw i
. array_init A 0
. matrix_init B 0
. array_set A i x
. row_set B i j
\end{asm}
\end{spasm}

\paragraph{Negated operators}
Currently there is only one negated operator supported. It translates
to two \Asm{} statements. 
\begin{spasm}
\begin{sparks}
z <- i != j
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp1 eqw i j
. set z not _tmp1
\end{asm}
\end{spasm}

\subsubsection{Array reads}
Array reads translate to one \Asm{} statement per array reference compared to the statements in Section~\ref{sec:simple}.
\begin{equation*}
  \steps(\codevar{array using expr}) = \steps(\codevar{basic expr}) + \#(\codevar{array refs}).
\end{equation*}

\begin{spasm}
\begin{sparks}
x <- A[i]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp1 array_ref A i
. set x copy _tmp1
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
x <- A[i] and A[j]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp3 array_ref A i
. set  _tmp4 array_ref A j
. set x and _tmp3 _tmp4
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
j <- B[[i]]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp6 row_ref B i
. set j copyw _tmp6
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
j <- B[[i]] + B[[k]]
\end{sparks}
  \tcblower
\begin{asm}
. set  _tmp8 row_ref B i
. set  _tmp9 row_ref B k
. set j addw _tmp8 _tmp9
\end{asm}
\end{spasm}

\subsection{Compound assignments}

For convenience \Sparks{} supports a single level of compound
expressions as right-hand-sides. In particular any right hand side
from Subsection~\ref{sec:simple} can be joined by an operator to any
other with matching type (i.e. \texttt{bool} or \texttt{int}).

\begin{equation*}
  \steps(\codevar{compound})=\steps(\codevar{rhs}) + \steps(\codevar{lhs}) + 1
\end{equation*}

\begin{spasm}
\begin{sparks}
i <- i + j + k
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp1 addw i j
. set _tmp2 copyw k
. set i addw _tmp1 _tmp2
\end{asm}
\end{spasm}
\begin{spasm}
\begin{sparks}
i <- i + j + k + j
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp4 addw i j
. set _tmp5 addw k j
. set i addw _tmp4 _tmp5
\end{asm}
\end{spasm}
\begin{spasm}
\begin{sparks}
z <-(x and y) 
      or (x and z)
\end{sparks}
  \tcblower
\begin{asm}
. set _tmp7 and x y
. set _tmp8 and x z
. set z or _tmp7 _tmp8
\end{asm}
\end{spasm}

\begin{spasm}
\begin{sparks}
z<-(A[0] and A[1]) 
   or (A[2] and A[3])
\end{sparks}
\tcblower
\begin{asm}
. set _tmp12 array_ref A 0
. set _tmp13 array_ref A 1
. set _tmp10 and _tmp12 _tmp13
. set _tmp14 array_ref A 2
. set _tmp15 array_ref A 3
. set _tmp11 and _tmp14 _tmp15
. set z or _tmp10 _tmp11
\end{asm}
\end{spasm}


\subsection{\texttt{if} blocks}
\label{sec:orgcedd1e2}
\begin{spasm}
\begin{sparks}
if `\BoolExpr` then
  `\Body`
endif
\end{sparks}
  \tcblower
\begin{asm}
. set guard0 `\BoolExpr`
. unless guard0 else0
  `\Body`
else0 `\ldots`
\end{asm}
\end{spasm}

\begin{equation*}
  \steps(\codevar{if}) \leq \steps(\Body) + \steps(\BoolExpr) + 2
\end{equation*}

\subsection{\texttt{if/else} blocks}
\label{sec:orgcedd1e2}
\begin{spasm}
\begin{sparks}
if `\BoolExpr` then
  `$\Body_1$`
else
  `$\Body_2$`
endif
\end{sparks}
  \tcblower
\begin{asm}
. set guard0 `\BoolExpr`
. unless guard0 else0
  `$\Body_1$`
. goto done0
else0 `$\Body_2$`
done0 `\ldots`
\end{asm}
\end{spasm}

\begin{equation*}
  \steps(\codevar{if-else}) \leq \max_i\steps(\Body_i) + \steps(\BoolExpr) + 3
\end{equation*}

\subsection{\texttt{for} loops}
\label{sec:org6801827}

\label{sec:org98fd16c}

\begin{spasm}
\begin{sparks}
for i<-`\Lower`,`\Upper` do
    `\Body`
done
\end{sparks}
\tcblower
\begin{asm}
. set i `\Lower`
. set _stop0 `\Upper`
for0 `\Body`
.    set _test0 eqw i _stop0
.    if _test0 done0
.    set i incw i
.    goto for0
done0 `\dots`
\end{asm}
\end{spasm}


\begin{equation*}
  \steps(\codevar{for}) = \steps(\Lower) + \steps(\Upper)
  + \sum_{i=\Lower}^{\Upper}\left(\steps(\Body;i)+4\right)-2
\end{equation*}


\subsection{\texttt{while} loops}

\begin{spasm}
\begin{sparks}
while `\BoolExpr`
      `\Body`
done
\end{sparks}
  \tcblower
\begin{asm}
while1 set _tmp2 `\BoolExpr`
.   set _test1 not _tmp2
.   unless _test1 done1
.   `\Body`
.   goto while1
done1 `\ldots`
\end{asm}
\end{spasm}

\begin{multline*}
  \steps(\codevar{while}) =   \sum_{i \in \mathrm{iterations}}\left(\steps(\BoolExpr)+\steps(\Body;i)+3\right) \\
  +  \steps(\BoolExpr)+2\\
\end{multline*}

\subsection{Generating inequalities from assembly}
\label{sec:constraints}

In the following tables, we give upper bounds for the number of
constraints generated by each assembly instruction. In general these can depend on the word size $W$ and the size of the any arrays ($r$) or matrices ($r,c$) involved.


\subsubsection{Control flow}

\begin{center}
  \begin{tabular}{l|l|r}
    \textsc{Sparks} & \textsc{Asm} & constraints \\\hline
\sac{if, while, for}{goto}{1}\\
\sac{}{if x label}{2}\\
\sac{}{unless x label}{2}\\
\sac{nop}{nop}{1}\\
\sac{return x @ $\beta$}{return x op args$\dots$}{1+cost (\lstinline[escapechar=,mathescape=true,escapebegin=]|set x op args $\ldots$|)}\\
\end{tabular}
\end{center}


\subsubsection{Set}

In the following samples, $i$, $j$, and $k$ are used for integer
variables and $x$, $y$, and $z$ are used for boolean variables.

\begin{center}
\begin{tabular}{l|l|r}
  \textsc{Sparks} & \textsc{Asm} & constraints \\\hline
  \sac{z <- x and y}{set z and x y}{3}\\
  \sac{z <- x}{set z copy x}{4}\\
  \sac{z <- x eq y}{set z eq x y}{4}\\
  \sac{z <- !x}{set z not x}{2}\\
  \sac{z <- x or y}{set z or x y}{3}\\
  \sac{z <- x xor y}{set z xor x y}{4}\\
  \sac{k <- j}{set k copyw j}{2W}\\
  \sac{k <- inc(i)}{set k incw i}{7W-8}\\
  \sac{k <- dec(i)}{set k decw i}{7W-8}\\
  \sac{z <- i = j}{set k eqw i j}{8W+3}\\
\end{tabular}
\end{center}

\subsubsection{Arrays}

\begin{center}
\begin{tabular}{l|l|r}
  \textsc{Sparks} & \textsc{Asm} & constraints \\\hline
  \sac{A[i] <- x}{array_set A i x}{$r(W+7)$}\\
  \sac{A[*] <- 1}{array_init A 1}{1}\\
  \sac{x <- A[i]}{set x array_ref A}{$r(W+3)$}
\end{tabular}
\end{center}

\subsubsection{Matrices}

\begin{center}
\begin{tabular}{l|l|r}
  \textsc{Sparks} & \textsc{Asm} & constraints \\\hline
  \sac{M[i,j] <- x}{matrix_set M i x}{$(r+c)(W+1)+6rc$}\\
  \sac{M[*,*] <- 1}{matrix_init M i j}{1}\\
  \sac{x <- A[i,j]}{set x matrix_ref M i j}{$(r+c)(W+1)+4rc$}
\end{tabular}
\end{center}
\subsubsection{Integer Arrays}

\begin{center}
\begin{tabular}{l|l|r}
  \textsc{Sparks} & \textsc{Asm} & constraints \\\hline
  \sac{M[[i]] <- j}{row_set M i j}{r(5W+1)}\\
  \sac{j <- M[[i]]}{set x row_ref M i}{r(3W+1)}
\end{tabular}
\end{center}


\section{Input files}
\label{sec:input}
Given a set of inequalities encoding a program, we still need to
encode the input to the program into an objective function.  These are
specified per bit, with a coefficient of $-2$ for each bit intended to
be zero and a coefficient of $+2$ for each bit intended to be one.
The user should also specify a coefficient for any output variables.
This coefficient can either be 0, or some sufficiently small positive
number.  The former case is less numerically problematic, while the
latter case allows one to read the value of the output variable from
the objective value.  Encoding an objective vector can be done
manually, but most people will want to use the supplied tool
\texttt{input2obj} which as the following usage.

\begin{verbatim}
input2obj [ <option> ... ] [<in-file>]
 where <option> is one of
  -w <ws>, --word-size <ws> : number of bits in integers
  -p <pf>, --param-file <pf> : param file
\end{verbatim}

The input format is similar to \textsc{Sparks}, with a matrix
initialization format similar to \texttt{C}.
\begin{sparksbox}
bool x <- 0
int m <- 3
array A[3] <- {1, 0, 1}
matrix M[3,3] <- { 0,0,0}, { 0, 1, 1}, {1,0,1}}
int array I[3] <- {1, 2, 3}
output bool w * 1/3
\end{sparksbox}
The \lstinline|* 1/3| specifies that the output variable should be given weight '1/3'
This weight on the output variable is entirely optional and defaults to
zero, i.e. \lstinline|output bool w| is equivalent to
\lstinline|output bool w * 0|.

\section{Sample Session}

\label{sec:sample}

The following sample sparks code reads in an array into the first row of a matrix, 
then copies and negates that to successive rows of the matrix.
The output is a particular bit in the resulting matrix.

\begin{sparksbox}
input array in[$n$]
output bool w
int i
int j
int k
matrix A[$n$,$n$]
for i<- 0,$n$ do
    A[0,i] <- in[i]
done
for i<- 1,$n$ do
    A[i,i]<-1
    for j<-0,$n$ do
	k <- dec(i)	
	A[i,j] <-  !A[k,j]
    done
done
return w @ A[$n$,$n$]
\end{sparksbox}

The following session can be reproduced by executing \texttt{make} in
the \texttt{examples/demo} subdirectory

\begin{verbatim}
# Run stages 1 and 2 of the compiler, generating a polytope that
# can run any $3$-bit input.
% ./bin/buildpoly -s matrix-for.sym matrix-for.spk \
                 matrix-for.param > matrix-for.lp

# Compile the objective function for a particular instance
% ./bin/input2obj -p matrix-for.param matrix-for.in > matrix-for.jso

# Run stage 3, adding an objective function, then solve lp
% ./bin/runpoly matrix-for.lp matrix-for.jso > matrix-for.out

# Use the symbols file generated by stage1 to plot sparks 
# source lines instead of asm
%./bin/plotlog -y 20 -s matrix-for.sym matrix-for.out > matrix-for.pdf
\end{verbatim}

The generated plot should look like Figure~\ref{fig:matrix-for}. The
program terminates on line 17 (the \lstinline|return|) around time
step 420.
\begin{figure}
  \begin{center}
  \includegraphics[height=0.37\textheight]{matrix-for.pdf}
  \end{center}
  \caption{Output of \texttt{plotlog} on \texttt{matrix-for.spk}}
  \label{fig:matrix-for}
\end{figure}
\section{Known limitations}

\begin{itemize}
\item The parameter file format is fragile (e.g. does not tolerate blank lines)
\item The default output from \texttt{plotlog} would be more intuitive
  if the $y$-axis was flipped.
\end{itemize}

\section{Release Notes}
\subsection{Release 2.0}

Major changes since 1.0 include arbitrarily large integer operations,
stricter type checking, and the tool \texttt{input2obj}, discussed
above.

\subsubsection{Arbitrarily large word size}

The parameter \texttt{word\_size} can now be set to any positive integer larger
than 1. The number of inequalities for integer operations scales
linearly with this parameter.

\subsubsection{Type checking}

The stage 1 compiler (\texttt{spk2asm}) is now more aggressive about
statically checking the types of operations. As a consequence of this

\begin{itemize}
\item All variables must now be declared before using.
\item Arithmetic macros are now typed integer. If you need booleans you can do something like
\end{itemize}
\begin{sparksbox}[mathescape=false]
bool x
x <- $complicated thing$ = 1
\end{sparksbox}

\appendix

\section{Internal representation of objective functions}
An objective function can also be given directly as a JSON file:
\begin{sparksbox}[language=JavaScript]
{
  "c": {
    "n[0]": -2, 
    "n[1]": 2, 
    "n[2]": -2
  }, 
  "d": {
    "w": 0
  }
}
\end{sparksbox}
Alternatively, the same data can be given in the GMPL data file
format.  Note that (confusingly) the usage of \emph{param} here has
nothing to do with the parameter files used by the stage 1 compiler
and \texttt{input2obj}.
\begin{sparksbox}
param d['w'] := 0;
param c :=
	'n[0]'	-2
	'n[1]'	2
	'n[2]'	-2
;
\end{sparksbox}


\section{Constraints for operations on variables}

In addition to the operations specified in Section 4 of \cite{ABTW} 
we have implemented some additional operations as described below.
Operations G(iii)-G(v) are reprinted from \cite{ABTW} as they
are used in the new operations.
Operation G(vi) was slightly simplified in the implementation and is given
here in revised form.

\begin{itemize}
\item[G(iii)] ({\bf assignment: $s=x \oplus y$})
Assume that $x,y,s$ are stored in $B(q,t-1), B(r,t-1), B(s,t)$ respectively.
\begin{eqnarray*}
S(i,t)+B(q,t-1)-B(r,t-1)-B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)-B(q,t-1)-B(r,t-1)+B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)-B(q,t-1)+B(r,t-1)-B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)+B(q,t-1)+B(r,t-1)+B(s,t) &\leqslant& 3~~~~~  
\end{eqnarray*}
If $S(i,t)=1$ then all constants on the right hand side are reduced by one and
$S(i,t)$ can be deleted. It is easy to check the inequalities have the
controlled $\{B(q,t-1),B(r,t-1)\}$-0/1 property, and that for each such 0/1 assignment $B(s,t)$
is correctly set.
\item[G(iv)] ({\bf assignment: $s=x \wedge y$})
Assume that $x,y,s$ are stored in $B(q,t-1), B(r,t-1), B(s,t)$ respectively.
\begin{eqnarray*}
S(i,t)-B(q,t-1)~~~~~~~~~~~~~~~~+B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)~~~~~~~~~~~~~~~~-B(r,t-1)+B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)+B(q,t-1)+B(r,t-1)-B(s,t) &\leqslant& 2~~~~~  
\end{eqnarray*}
If $S(i,t)=1$ then all constants on the right hand side are reduced by one and
$S(i,t)$ can be deleted. It is easy to check the inequalities have the
controlled $\{B(q,t-1),B(r,t-1)\}$-0/1 property, and that for each such 0/1 assignment $B(s,t)$
is correctly set.
\item[G(v)] ({\bf assignment: $s=x \vee y$, $s=x_1 \vee x_2 \vee ... \vee x_k$})\\
Assume that $x,y,s$ are stored in $B(q,t-1), B(r,t-1), B(s,t)$ respectively.
\begin{eqnarray*}
S(i,t)+B(q,t-1)~~~~~~~~~~~~~~~~-B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)~~~~~~~~~~~~~~~~+B(r,t-1)-B(s,t) &\leqslant& 1~~~~~  \\
S(i,t)-B(q,t-1)-B(r,t-1)+B(s,t) &\leqslant& 1~~~~~
\end{eqnarray*}
The analysis is similar to G(iv) and is omitted.
The inequalities have the controlled \{B(q,t-1),B(r,t-1)\}-0/1 property.

The $k$-way {\em or} is an easy generalization
which will be needed in the sequel, where we assume that
$x_j$ is stored in $B(q_j,t-1), j=1,2,...,k$. It is defined
by the following inequalities:
\begin{eqnarray*}
S(i,t)+B(q_j,t-1)-B(s,t) &\leqslant& 1~~~~~1 \leqslant j \leqslant k  \\
S(i,t)-\sum_{j=1}^k B(q_j,t-1) + B(s,t) &\leqslant& 1.
\end{eqnarray*}
\item[G(vi)] ({\bf increment integer variable})
Assume that the integer variable is stored in $I(q,j,t-1)$, $ 1 \leqslant j \leqslant W$ and
is to be incremented by 1. If $W>2$ we require $W-2$ temporary variables, 
$B(j,t), 1 \leqslant j \leqslant W-2$,
to hold the binary carries.
On overflow 
$I(q,j,t)=0, 1 \leqslant j \leqslant W$.
The incrementer makes use of two previous operations, G(iii) and G(iv).
For the first 2 bits of output we do not need any carry bits as the low order
input bit is the carry. We implement this by:
\begin{eqnarray*}
~~S(i,t)-I(q,1,t-1) -I(q,1,t)  \leqslant 0~~~~~  \\
~~S(i,t)+I(q,1,t-1) +I(q,1,t)  \leqslant 2~~~~~  \\
I(q,2,t) =  I(q,2,t-1) \oplus   I(q,1,t-1)
\end{eqnarray*}
The first two inequalities give the equation $I(q,1,t-1)+I(q,1,t)=1$ when $S(i,j)=1$.\\
When $W \ge 3$ we have in addition for $3 \leqslant j \leqslant W$:
\begin{eqnarray*}
B(j-2,t)&=& I(q,j,t-1) \wedge I(q,j-1,t-1)  \\ 
I(q,j,t)&=& I(q,j,t-1) \oplus   B(j-2,t)  
\end{eqnarray*}
By appropriate
formal substitution of variables, each of the above
assignments is transformed into inequalities of the form G(iii) and G(iv), which are
controlled by the step counter $S(i,t)$. It can be verified that the full system
satisfies the controlled $\{I(q,j,t-1), 1 \leqslant j \leqslant W\}$-0/1 
property because for each 0/1 setting
of these variables all other variables are fixed by the above system of equations.
There are a total of $7W-1$ inequalities.

\item[G(xi)] ({\bf strict less than test for integer variables})
Assume that the integer variables are stored in $I(q,j,t-1)$ and
$I(r,j,t-1)$, $1 \leqslant j \leqslant W$. 
We require $4W-3$ temporary variables $B(j,t),B_1(j,t),B_2(j,t),B_3(j,t),2 \leqslant j \leqslant W$
and $B(W+1,t)$.
If the first integer variable is strictly less than the second
then $B(W+1,t)$ is set to one else it is set to zero.
We define:
\begin{eqnarray*}
B(2,t) = \neg I(q,1,t-1) \wedge I(r,1,t-1)
\end{eqnarray*}
and for $W \ge 2$ we have in addition for $2 \leqslant j \leqslant W$:
\begin{eqnarray*}
B_1(j,t) &=& \neg I(q,j,t-1)\wedge I(r,j,t-1)  \\
B_2(j,t) &=&  I(q,j,t-1)\oplus I(r,j,t-1)  \\
B_3(j,t) &=& \neg B_2(j,t)\wedge B(j,t)  \\
B(j+1,t) &=&  B_1(j,t)\vee   B_3(j,t) 
\end{eqnarray*}
The second equation makes use of G(iii) and the others use G(iv), after appropriate substitutions.
These equations, which start from the least significant bit, were given in \cite{ST04}
and generate $13W-10$ inequalities.
The inequalities have the controlled \{$I(q,j,t-1), I(r,j,t-1)$, $1 \leqslant j \leqslant W$\}-0/1 property.
\item[G(xii)] ({\bf array and matrix initialization})
Assume that the array or matrix is stored in $B(\alpha+j,t),0 \le j \le u$ and all entries
are to be initialized
to either 0 or 1 at time $t$. This can be achieved by the single inequality
\begin{eqnarray}
\label{zero}
S(i,t)+\sum_{j=0}^u B(\alpha+j,t) \leq 1
\end{eqnarray}
for initializing to zero, and by
\begin{eqnarray}
\label{one}
(u+1)S(i,t)-\sum_{j=0}^u B(\alpha+j,t) \le 0
\end{eqnarray}
for initializing to one.

({\bf integer array initialization})
Assume an integer array stored as a matrix I[n,wordsize].
Using inequality (\ref{zero}) it can be initialized to zero at time $t$ and
using inequality (\ref{one}) it can be initialized to the maximum integer, $2^W -1$.
By treating each column as a bit array, 
a single fixed $W$-bit compile time constant can be assigned to each row of $I[n,*]$.

\item[G(xiii)] ({\bf equality test for integer variable and constant})
Assume that the integer variable is stored in $I(q,j,t-1), j=1,...,W$ and the compile time constant
has binary expansion $b_1,...,b_W$. 
If the variable equals the constant the temporary variable $B(1,t)=1$ else it is zero.
The test G(vii) can be replaced by the $W+1$ inequalities:
\begin{eqnarray*}
S(i,t) + B(1,t)+I(q,j,t-1) &\le& 2~~~~~~~(b_j = 0 ) \\
S(i,t) + B(1,t)-I(q,j,t-1) &\le& 1~~~~~~~(b_j = 1 )  \\
S(i,t) - B(1,t)-\sum_{b_j=0} I(q,j,t-1) - \sum_{b_j=1}(1- I(q,j,t-1)) &\le& 0 \\
\end{eqnarray*}

\item[G(xiv)] ({\bf addition of integer variables})
Assume that the integer variables to be added are stored in $I(x,j,t-1)$ and
$I(y,j,t-1)$, $1 \leqslant j \leqslant W$. We produce an output integer 
$I(z,j,t)$, $1 \leqslant j \leqslant W$.
We require $4W-3$ temporary variables $B(j,t), 1 \leqslant j \leqslant W$,
and $B_1(j,t),B_2(j,t),B_3(j,t), 2 \leqslant j \leqslant W$. 
The $B(j,t)$ variables hold the carry bits.
We define:
\begin{eqnarray*}
I(z,1,t) =  I(x,1,t-1) \oplus I(y,1,t-1) \\
B(1,t)   =  I(x,1,t-1) \wedge I(y,1,t-1)
\end{eqnarray*}
and for $2 \leqslant j \leqslant W$:
\begin{eqnarray*}
B_1(j,t) &=&  I(x,j,t-1)\wedge I(y,j,t-1)  \\
B_2(j,t) &=&  I(x,j,t-1)\oplus I(y,j,t-1)  \\
B_3(j,t) &=&  B(j-1,t)\wedge B_2(j,t) \\
I(z,j,t)   &=&  B_2(j,t)\oplus B(j-1,t) \\
B(j,t)   &=&  B_1(j,t)\vee   B_3(j,t)
\end{eqnarray*}
The $\oplus$ operation was described in G(iii) and requires 4 inequalities.
The $\wedge$ and $\vee$ operations were described in G(iv) and G(v) respectively
and each requires 3 inequalities. 
In total $17W-10$ inequalities are required.
The inequalities have the controlled \{$I(x,j,t-1), I(y,j,t-1)$, $1 \leqslant j \leqslant W$\}-0/1 property.

\end{itemize}


\subsection{Integer array operations}

It turns out to be quite inefficient to translate an integer array
operation into a set of bit operations. We therefore provide assembly
instructions for reading and writing a row of a matrix to and from an
integer variable.
The assignment constraints are derived in a straightforward way from G.ix (one can imagine $N(\dot,t)$ is constant zero indicating selection. With notation as in G.ix, we have:
\begin{eqnarray*}
S(i,t)+B(x,t-1)-B(\alpha + r,t) -M(j_1,t) &\leqslant& 1 \\
S(i,t)-B(x,t-1)+B(\alpha + r,t) -M(j_1,t) &\leqslant& 1 \\
S(i,t)+B(\alpha + r,t-1)-B(\alpha + r,t) +M(j_1,t) &\leqslant& 2 \\
S(i,t)-B(\alpha + r,t-1)+B(\alpha + r,t) +M(j_1,t) &\leqslant& 2 \\
\end{eqnarray*}

Reading a row into an integer variable we have
\begin{eqnarray*}
S(i,t)+B(x,t)-B(\alpha + r,t-1) - M(j_1,t) &\leqslant& 1 \\
S(i,t)-B(x,t)+B(\alpha + r,t-1) - M(j_1,t) &\leqslant& 1
\end{eqnarray*}

\section{Optimizations to constraints}

\subsection{Grouping $S[i,t]$}
We observed that certain groups of constraints in our original
formulation were identical except for the index $i$ (i.e.\ the source
line number) in the control variable $S[i,t]$. Although this apparent
duplication represents only a constant blowup, in our experiments this
factor is roughly $250$, which is noticable in terms of problem size.
To more efficiently encode some of these constraint sets, we
constructed control variables representing groups of source lines; in
particular for \textsc{Sparks} variable $v$, we construct variables
\begin{align*}
  I[v,t] & = \vee_{i \mid \text{$v$ used as index on line $i$}} S[i,t]\\
  K[v,t] & = \neg \vee_{i \mid \text{$v$ modified on line $i$}} S[i,t]
\end{align*}
The $I$ variables are used to control constraints G.viii and G.ix, in
particular those constraints enforcing the values of the $M$ and $N$
variables representing binary decoded row/column indices.  The $K$
variables are used to control the G.i constraints, and (the carry forward part of) the new row assignment constraints described above.
The negation of the
$K[v,t]$ reflects the fact that the $S[i,t]$ in the constraints
defining them are the complement (w.r.t.\ index $i$ of those
controlling the original G.i constraints)

\bibliography{manual}

\end{document}
